//
//  HanekeView.swift
//  VietThucSwift_Example
//
//  Created by Tran Viet Thuc on 4/20/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import VietThucSwift

class HanekeView: BaseView,  UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBAction func _btnClearCacheClick(_ sender: Any) {
        self.items.removeAll()
        _collectionView.reloadData()
        Shared.imageCache.removeAllWithOutDirectory()
        Shared.dataCache.removeAllWithOutDirectory()
        Shared.JSONCache.removeAllWithOutDirectory()
        
        //Shared.imageCache.removeAllWithDirectory()
        //Shared.dataCache.removeAllWithDirectory()
        //Shared.JSONCache.removeAllWithDirectory()
    }
    
    init(frame: CGRect, rootVC: BaseViewController) {
        super.init(frame: frame)
        self.xibSetup(frame: frame, rootVC: rootVC)
        self.initCollectionView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func willMove(toWindow newWindow: UIWindow?) {
        super.willMove(toWindow: newWindow)
        if newWindow == nil {// UIView disappear
        } else {// UIView appear
        }
    }
    
    func cacheVideos() {
        let cache = Shared.dataCache
        //Save
        cache.set(value: Data(), key: "funny-games.mp4")
        
        //Read
        cache.fetch(key: "funny-games.mp4").onSuccess( { data in
            // Do something with data
        }).onFailure( { (error) in
            
        })
    }
    
    func cacheJSON() {
        let cache = Shared.JSONCache
        //Save
        cache.set(value: JSON.Dictionary([:]), key: "https://api.github.com/users/haneke")
        
        //Read
        cache.fetch(URL: URL(string: "https://api.github.com/users/haneke")!).onSuccess( { json in
            print(json.dictionary.debugDescription)
        }).onFailure( { (error) in
            
        })
    }
    
    func cacheImage() {
        let cache = Shared.imageCache
        //Save
        cache.set(value: UIImage(), key: "https://raw.githubusercontent.com/Haneke/HanekeSwift/master/Assets/github-header.png")
        
        //Read
        cache.fetch(URL: URL(string: "https://raw.githubusercontent.com/Haneke/HanekeSwift/master/Assets/github-header.png")!, formatName: "icons").onSuccess( { image in
            // image will be a nice rounded icon
        }).onFailure( { (error) in
            
        })
        
        // Setting a remote image
        //imageView.hnk_setImageFromURL(url)
        
        // Setting an image manually. Requires you to provide a key.
        //imageView.hnk_setImage(image, key: key)
    }
    
    //<key>NSAppTransportSecurity</key>
    //<dict>
    //<key>NSAllowsArbitraryLoads</key>
    //<true/>
    //</dict>
    
    //CollectionView
    @IBOutlet weak var _collectionView: UICollectionView!
    private var items : [String] = []
    func initCollectionView() {
        _collectionView.delegate = self
        _collectionView.dataSource = self
        
        _collectionView!.register(HanekeCell.self, forCellWithReuseIdentifier: "Cell")
        let layout = _collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: 100, height: 100)
        layout.minimumLineSpacing = 5.0
        layout.minimumInteritemSpacing = 5.0
        layout.scrollDirection = .vertical
        
        self.initializeItemsWithURLs()
        //_collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! HanekeCell
        cell.backgroundColor = .green
        
        let URLString = self.items[(indexPath as NSIndexPath).row]
        let url = URL(string:URLString)!
        cell.imageView.hnk_setImageFromURL(url)
        return cell
    }
    //CollectionView ./
    
    func initializeItemsWithURLs() {
        items = ["http://imgs.xkcd.com/comics/election.png",
                 "http://imgs.xkcd.com/comics/scantron.png",
                 "http://imgs.xkcd.com/comics/secretary_part_5.png",
                 "http://imgs.xkcd.com/comics/secretary_part_4.png",
                 "http://imgs.xkcd.com/comics/secretary_part_3.png",
                 "http://imgs.xkcd.com/comics/secretary_part_2.png",
                 "http://imgs.xkcd.com/comics/secretary_part_1.png",
                 "http://imgs.xkcd.com/comics/actuarial.png",
                 "http://imgs.xkcd.com/comics/scrabble.png",
                 "http://imgs.xkcd.com/comics/twitter.png",
                 "http://imgs.xkcd.com/comics/morning_routine.png",
                 "http://imgs.xkcd.com/comics/going_west.png",
                 "http://imgs.xkcd.com/comics/steal_this_comic.png",
                 "http://imgs.xkcd.com/comics/numerical_sex_positions.png",
                 "http://imgs.xkcd.com/comics/i_am_not_a_ninja.png",
                 "http://imgs.xkcd.com/comics/depth.png",
                 "http://imgs.xkcd.com/comics/flash_games.png",
                 "http://imgs.xkcd.com/comics/fiction_rule_of_thumb.png",
                 "http://imgs.xkcd.com/comics/height.png",
                 "http://imgs.xkcd.com/comics/listen_to_yourself.png",
                 "http://imgs.xkcd.com/comics/spore.png",
                 "http://imgs.xkcd.com/comics/tones.png",
                 "http://imgs.xkcd.com/comics/the_staple_madness.png",
                 "http://imgs.xkcd.com/comics/typewriter.png",
                 "http://imgs.xkcd.com/comics/one-sided.png",
                 "http://imgs.xkcd.com/comics/further_boomerang_difficulties.png",
                 "http://imgs.xkcd.com/comics/turn-on.png",
                 "http://imgs.xkcd.com/comics/still_raw.png",
                 "http://imgs.xkcd.com/comics/house_of_pancakes.png",
                 "http://imgs.xkcd.com/comics/aversion_fads.png",
                 "http://imgs.xkcd.com/comics/the_end_is_not_for_a_while.png",
                 "http://imgs.xkcd.com/comics/improvised.png",
                 "http://imgs.xkcd.com/comics/fetishes.png",
                 "http://imgs.xkcd.com/comics/x_girls_y_cups.png",
                 "http://imgs.xkcd.com/comics/moving.png",
                 "http://imgs.xkcd.com/comics/quantum_teleportation.png",
                 "http://imgs.xkcd.com/comics/rba.png",
                 "http://imgs.xkcd.com/comics/voting_machines.png",
                 "http://imgs.xkcd.com/comics/freemanic_paracusia.png",
                 "http://imgs.xkcd.com/comics/google_maps.png",
                 "http://imgs.xkcd.com/comics/paleontology.png",
                 "http://imgs.xkcd.com/comics/holy_ghost.png",
                 "http://imgs.xkcd.com/comics/regrets.png",
                 "http://imgs.xkcd.com/comics/frustration.png",
                 "http://imgs.xkcd.com/comics/cautionary.png",
                 "http://imgs.xkcd.com/comics/hats.png",
                 "http://imgs.xkcd.com/comics/rewiring.png",
                 "http://imgs.xkcd.com/comics/upcoming_hurricanes.png",
                 "http://imgs.xkcd.com/comics/mission.png",
                 "http://imgs.xkcd.com/comics/impostor.png",
                 "http://imgs.xkcd.com/comics/the_sea.png",
                 "http://imgs.xkcd.com/comics/things_fall_apart.png",
                 "http://imgs.xkcd.com/comics/good_morning.png",
                 "http://imgs.xkcd.com/comics/too_old_for_this_shit.png",
                 "http://imgs.xkcd.com/comics/in_popular_culture.png",
                 "http://imgs.xkcd.com/comics/i_am_not_good_with_boomerangs.png",
                 "http://imgs.xkcd.com/comics/macgyver_gets_lazy.png",
                 "http://imgs.xkcd.com/comics/know_your_vines.png",
                 "http://imgs.xkcd.com/comics/xkcd_loves_the_discovery_channel.png",
                 "http://imgs.xkcd.com/comics/babies.png",
                 "http://imgs.xkcd.com/comics/road_rage.png",
                 "http://imgs.xkcd.com/comics/thinking_ahead.png",
                 "http://imgs.xkcd.com/comics/internet_argument.png",
                 "http://imgs.xkcd.com/comics/suv.png",
                 "http://imgs.xkcd.com/comics/how_it_happened.png",
                 "http://imgs.xkcd.com/comics/purity.png",
                 "http://imgs.xkcd.com/comics/xkcd_goes_to_the_airport.png",
                 "http://imgs.xkcd.com/comics/journal_5.png",
                 "http://imgs.xkcd.com/comics/journal_4.png",
                 "http://imgs.xkcd.com/comics/delivery.png",
                 "http://imgs.xkcd.com/comics/every_damn_morning.png",
                 "http://imgs.xkcd.com/comics/fantasy.png",
                 "http://imgs.xkcd.com/comics/starwatching.png",
                 "http://imgs.xkcd.com/comics/bad_timing.png",
                 "http://imgs.xkcd.com/comics/geohashing.png",
                 "http://imgs.xkcd.com/comics/fortune_cookies.png",
                 "http://imgs.xkcd.com/comics/security_holes.png",
                 "http://imgs.xkcd.com/comics/finish_line.png",
                 "http://imgs.xkcd.com/comics/a_better_idea.png",
                 "http://imgs.xkcd.com/comics/making_hash_browns.png",
                 "http://imgs.xkcd.com/comics/jealousy.png",
                 "http://imgs.xkcd.com/comics/forks_and_spoons.png",
                 "http://imgs.xkcd.com/comics/stove_ownership.png",
                 "http://imgs.xkcd.com/comics/the_man_who_fell_sideways.png",
                 "http://imgs.xkcd.com/comics/zealous_autoconfig.png",
                 "http://imgs.xkcd.com/comics/restraining_order.png",
                 "http://imgs.xkcd.com/comics/mistranslations.png",
                 "http://imgs.xkcd.com/comics/new_pet.png",
                 "http://imgs.xkcd.com/comics/startled.png",
                 "http://imgs.xkcd.com/comics/techno.png",
                 "http://imgs.xkcd.com/comics/math_paper.png",
                 "http://imgs.xkcd.com/comics/electric_skateboard_double_comic.png",
                 "http://imgs.xkcd.com/comics/overqualified.png",
                 "http://imgs.xkcd.com/comics/cheap_gps.png",
                 "http://imgs.xkcd.com/comics/venting.png",
                 "http://imgs.xkcd.com/comics/journal_3.png",
                 "http://imgs.xkcd.com/comics/convincing_pickup_line.png",
                 "http://imgs.xkcd.com/comics/1000_miles_north.png",
                 "http://imgs.xkcd.com/comics/large_hadron_collider.png",
                 "http://imgs.xkcd.com/comics/important_life_lesson.png"]
    }
}
