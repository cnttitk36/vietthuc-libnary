//
//  FullCollectionView.swift
//  VietThucSwift_Example
//
//  Created by Tran Viet Thuc on 4/15/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import VietThucSwift

class FullCollectionView: BaseView, UICollectionViewDelegate, UICollectionViewDataSource, BaseCollectionViewLayoutDelegate {
    
    @IBOutlet weak var _collectionView: UICollectionView!
    var listXe: Array<XeModel>?
    
    func initDataBaoDuong() -> SumnaryModel{
        var listData = Array<String>.init()
        listData.append("STT")
        listData.append("Nội dung bảo trì")
        listData.append("Số lít/số lượng")
        listData.append("Chi phí")
        listData.append("Số km hiện tại")
        listData.append("Ngày bảo trì")
        listData.append("Ghi chú")
        
        listData.append("1")
        listData.append("02/04/2018")
        listData.append("Bridgestone R150 TL-TCF")
        listData.append("11.00R20")
        listData.append("tube type")
        listData.append("Y7L314945-2517")
        listData.append("trục 1;1")
        
        listData.append("2")
        listData.append("02/04/2018")
        listData.append("Bridgestone R150 TL-TCF")
        listData.append("11.00R20")
        listData.append("tube type")
        listData.append("Y7L314945-2517")
        listData.append("trục 1;1")
        
        return SumnaryModel.init(listData: listData)
    }
    
    func initDataXangDau() -> SumnaryModel{
        var listData = Array<String>.init()
        listData.append("STT")
        listData.append("Số km hiện tại")
        listData.append("Ngày nạp")
        listData.append("Số lượng (lít)")
        listData.append("Đơn giá (theo lít)")
        listData.append("Chi phí")
        
        listData.append("1")
        listData.append("02/04/2018")
        listData.append("Bridgestone R150 TL-TCF")
        listData.append("11.00R20")
        listData.append("tube type")
        listData.append("Y7L314945-2517")
        
        listData.append("2")
        listData.append("02/04/2018")
        listData.append("Bridgestone R150 TL-TCF")
        listData.append("11.00R20")
        listData.append("tube type")
        listData.append("Y7L314945-2517")
        
        return SumnaryModel.init(listData: listData)
    }
    
    func initDataThayNhot() -> SumnaryModel{
        var listData = Array<String>.init()
        listData.append("STT")
        listData.append("Ngày Thay Thế")
        listData.append("Số km Hiện Tại")
        listData.append("Số km Đi Được")
        listData.append("Hiệu Lốp")
        listData.append("Size")
        listData.append("Loại Lốp")
        listData.append("SeriesDate")
        listData.append("Vị Trí Lốp")
        listData.append("Số Lượng")
        listData.append("Chi Phí")
        
        listData.append("1")
        listData.append("02/04/2018")
        listData.append("222")
        listData.append("50")
        listData.append("AAA")
        listData.append("45")
        listData.append("BBB")
        listData.append("02/04/2018")
        listData.append("Trước")
        listData.append("01")
        listData.append("1.500.000 VNĐ")
        
        listData.append("2")
        listData.append("02/04/2018")
        listData.append("222")
        listData.append("50")
        listData.append("AAA")
        listData.append("45")
        listData.append("BBB")
        listData.append("02/04/2018")
        listData.append("Trước")
        listData.append("01")
        listData.append("1.500.000 VNĐ")
        
        return SumnaryModel.init(listData: listData)
    }
    
    func initDataSuaChua() -> SumnaryModel{
        var listData = Array<String>.init()
        listData.append("STT")
        listData.append("Nội dung bảo trì")
        listData.append("Số lít/số lượng")
        listData.append("Chi phí")
        listData.append("Số km hiện tại")
        listData.append("Ngày bảo trì")
        listData.append("Ghi chú")
        
        listData.append("1")
        listData.append("02/04/2018")
        listData.append("Bridgestone R150 TL-TCF")
        listData.append("11.00R20")
        listData.append("tube type")
        listData.append("Y7L314945-2517")
        listData.append("trục 1;1")
        
        listData.append("2")
        listData.append("02/04/2018")
        listData.append("Bridgestone R150 TL-TCF")
        listData.append("11.00R20")
        listData.append("tube type")
        listData.append("Y7L314945-2517")
        listData.append("trục 1;1")
        
        return SumnaryModel.init(listData: listData)
    }
    
    func initDataThayLopXe() -> SumnaryModel{
        var listData = Array<String>.init()
        listData.append("STT")
        listData.append("Ngày thay thế")
        listData.append("Hiệu lốp")
        listData.append("Size")
        listData.append("Loại lốp")
        listData.append("Series-date")
        listData.append("Vị trí lốp")
        listData.append("Số lượng")
        listData.append("Chi phí")
        listData.append("Số km hiện tại")
        
        listData.append("1")
        listData.append("02/04/2018")
        listData.append("Bridgestone R150 TL-TCF")
        listData.append("11.00R20")
        listData.append("tube type")
        listData.append("Y7L314945-2517")
        listData.append("trục 1;1")
        listData.append("100")
        listData.append("50")
        listData.append("94582")
        
        listData.append("2")
        listData.append("02/04/2018")
        listData.append("Bridgestone R150 TL-TCF")
        listData.append("11.00R20")
        listData.append("tube type")
        listData.append("Y7L314945-2517")
        listData.append("trục 1;1")
        listData.append("100")
        listData.append("50")
        listData.append("94582")
        
        return SumnaryModel.init(listData: listData)
    }
    
    func initData() {
        //indexSelected, currentIndex == 0 --> Bảo dưỡng | 1 --> Xăng Dầu | 2 --> Thay Nhớt | 3 --> Sửa Chữa | 4 --> Thay Lốp Xe
        listXe = Array<XeModel>.init()
        listXe?.append(XeModel.init(title: "XE: 51C-773.96", date: "Ngày 11/05/2018", amount: "Tổng chi phí: 20.000.000 VNĐ", listBaoDuong: initDataBaoDuong(), listXangDau: initDataXangDau(), listThayNhot: initDataThayNhot(), listSuaChua: initDataSuaChua(), listThayLopXe: initDataThayLopXe(), currentIndex: 0))
        
        listXe?.append(XeModel.init(title: "XE: 77C-773.96", date: "Ngày 12/05/2018", amount: "Tổng chi phí: 30.000.000 VNĐ", listBaoDuong: initDataBaoDuong(), listXangDau: initDataXangDau(), listThayNhot: initDataThayNhot(), listSuaChua: initDataSuaChua(), listThayLopXe: initDataThayLopXe(), currentIndex: 0))
        
        listXe?.append(XeModel.init(title: "XE: 78C-773.96", date: "Ngày 13/05/2018", amount: "Tổng chi phí: 40.000.000 VNĐ", listBaoDuong: initDataBaoDuong(), listXangDau: initDataXangDau(), listThayNhot: initDataThayNhot(), listSuaChua: initDataSuaChua(), listThayLopXe: initDataThayLopXe(), currentIndex: 0))
    }
    
    init(frame: CGRect, rootVC: BaseViewController) {
        super.init(frame: frame)
        self.xibSetup(frame: frame, rootVC: rootVC)
        
        self.initData()
        self.initCollectionView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func getDeviceOrientation() -> UICollectionView.ScrollDirection {
        switch UIDevice.current.orientation{
        case .portrait:
            print("Portrait")
            return .horizontal
        case .portraitUpsideDown:
            print("Portrait upside down")
            return .horizontal
        case .landscapeLeft:
            print("Landscape left")
            return .vertical
        case .landscapeRight:
            print("Landscape right")
            return .vertical
        default:
            print("Another")
            return .vertical
        }
    }
    
    override func willMove(toWindow newWindow: UIWindow?) {
        super.willMove(toWindow: newWindow)
        if newWindow == nil {// UIView disappear
        } else {// UIView appear
        }
    }
    
    //CollectionView
    func initCollectionView() {
        _collectionView.delegate = self
        _collectionView.dataSource = self
        
        let addNibName = UINib(nibName: "SumCell", bundle: nil)
        _collectionView.register(addNibName, forCellWithReuseIdentifier: "sumCell")
        let headerNib = UINib(nibName: "HeaderCell", bundle: nil)
        _collectionView.register(headerNib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "headerCell")
        let footerNib = UINib(nibName: "FooterCell", bundle: nil)
        _collectionView.register(footerNib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "footerCell")
        
        let baseLayout = BaseCollectionViewLayout()
        baseLayout.scrollDirection = self.getDeviceOrientation()
        baseLayout.delegate = self
        _collectionView.collectionViewLayout = baseLayout
        _collectionView.isDirectionalLockEnabled = true
        
        _collectionView.layoutIfNeeded()
    }
    
    //Số lượng Section
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.listXe?.count ?? 0
    }
    
    //Số lượng item trong section
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.listXe?[section].currentIndex == 0 {
            return self.listXe?[section].listBaoDuong?.listData?.count ?? 0
        }else if self.listXe?[section].currentIndex == 1 {
            return self.listXe?[section].listXangDau?.listData?.count ?? 0
        }else if self.listXe?[section].currentIndex == 2 {
            return self.listXe?[section].listThayNhot?.listData?.count ?? 0
        }else if self.listXe?[section].currentIndex == 3 {
            return self.listXe?[section].listSuaChua?.listData?.count ?? 0
        }else if self.listXe?[section].currentIndex == 4 {
            return self.listXe?[section].listThayLopXe?.listData?.count ?? 0
        }
        return 0
    }
    
    //Khoảng cách giữa các Section
    func customViewLayout(colletionViewLayout: BaseCollectionViewLayout, insetForSection section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: 0, bottom: 10, right: 0)
    }
    
    //Kích thước Header trong section
    func customViewLayout(collectionViewLayout: BaseCollectionViewLayout, sizeForHeaderSection section: Int) -> CGSize {
        return CGSize(width:_collectionView.frame.size.width, height: 80.0)
    }
    
    //Số lượng cột trong Section
    func customViewLayout(collecitionViewLayout: BaseCollectionViewLayout, numberColsInSection section: Int) -> Int {
        if self.listXe?[section].currentIndex == 0 {
            return 7
        }else if self.listXe?[section].currentIndex == 1 {
            return 6
        }else if self.listXe?[section].currentIndex == 2 {
            return 11
        }else if self.listXe?[section].currentIndex == 3 {
            return 7
        }else if self.listXe?[section].currentIndex == 4 {
            return 10
        }
        return 0
    }
    
    //Kích thước Cell tại dòng indexPath.row
    func customViewLayout(collecitionViewLayout: BaseCollectionViewLayout, sizeForItem indexPath: IndexPath) -> CGSize {
        return CGSize(width: 120.0, height: 40.0)
    }
    
    //Kích thước Footer trong section
    func customViewLayout(collectionViewLayout: BaseCollectionViewLayout, sizeForFooterSection section: Int) -> CGSize {
        return CGSize(width: _collectionView.frame.size.width, height: 60.0)
    }
    
    //Giao diện Cell tại dòng indexPath.row
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sumCell", for: indexPath) as! SumCell
        cell.backgroundColor = UIColor.lightGray
        if self.listXe?[indexPath.section].currentIndex == 0 {
            cell.setupView(self.listXe?[indexPath.section].listBaoDuong?.listData?[indexPath.row])
        }else if self.listXe?[indexPath.section].currentIndex == 1 {
            cell.setupView(self.listXe?[indexPath.section].listXangDau?.listData?[indexPath.row])
        }else if self.listXe?[indexPath.section].currentIndex == 2 {
            cell.setupView(self.listXe?[indexPath.section].listThayNhot?.listData?[indexPath.row])
        }else if self.listXe?[indexPath.section].currentIndex == 3 {
            cell.setupView(self.listXe?[indexPath.section].listSuaChua?.listData?[indexPath.row])
        }else if self.listXe?[indexPath.section].currentIndex == 4 {
            cell.setupView(self.listXe?[indexPath.section].listThayLopXe?.listData?[indexPath.row])
        }
        return cell
    }
    
    //Giao diện Header + Footer
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headerCell", for: indexPath) as! HeaderCell
            headerView.commoInnit(title: self.listXe?[indexPath.section].title, view: self, currentSection: indexPath.section, indexSelected: self.listXe?[indexPath.section].currentIndex)
            return headerView
        case UICollectionView.elementKindSectionFooter:
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "footerCell", for: indexPath) as! FooterCell
            footerView.commoInnit(date: self.listXe?[indexPath.section].date, amount: self.listXe?[indexPath.section].amount)
            return footerView
        default://I only needed a header so if not header I return an empty view
            return UICollectionReusableView()
        }
    }
    
    func updateList(currentSection: Int, indexSelected: Int) {
        self.listXe?[currentSection].currentIndex = indexSelected
        _collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //Log.debug("Selected: \(indexPath.item)\n")
    }
    //CollectionView ./
}
