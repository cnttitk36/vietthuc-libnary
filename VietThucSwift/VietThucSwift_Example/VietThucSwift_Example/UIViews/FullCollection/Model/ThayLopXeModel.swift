//
//  ThayLopXeModel.swift
//  DemoMobisApplication
//
//  Created by boys vip on 4/14/18.
//  Copyright © 2018 boys vip. All rights reserved.
//

import Foundation

public class ThayLopXeModel: NSObject {
    var sTT: String?
    var ngayThayThe: String?
    var hieuLop: String?
    var size: String?
    var loaiLop: String?
    var series_Date: String?
    var viTriLop: String?
    var soLuong: String?
    var chiPhi: String?
    var soKmHienTai: String?
    
    init(sTT: String?, ngayThayThe: String?, hieuLop: String?, size: String?, loaiLop: String?, series_Date: String?, viTriLop: String?, soLuong: String?, chiPhi: String?, soKmHienTai: String?) {
        self.sTT = sTT
        self.ngayThayThe = ngayThayThe
        self.hieuLop = hieuLop
        self.size = size
        self.loaiLop = loaiLop
        self.series_Date = series_Date
        self.viTriLop = viTriLop
        self.soLuong = soLuong
        self.chiPhi = chiPhi
        self.soKmHienTai = soKmHienTai
    }
}
