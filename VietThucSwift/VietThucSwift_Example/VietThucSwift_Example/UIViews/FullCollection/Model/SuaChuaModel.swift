//
//  SuaChuaModel.swift
//  DemoMobisApplication
//
//  Created by boys vip on 4/14/18.
//  Copyright © 2018 boys vip. All rights reserved.
//

import Foundation

public class SuaChuaModel: NSObject {
    var sTT: String?
    var noiDungBaoTri: String?
    var soLit: String?
    var chiPhi: String?
    var soKmHienTai: String?
    var ngayBaoTri: String?
    var ghiChu: String?
    
    init(sTT: String?, noiDungBaoTri: String?, soLit: String?, chiPhi: String?, soKmHienTai: String?, ngayBaoTri: String?, ghiChu: String?) {
        self.sTT = sTT
        self.noiDungBaoTri = noiDungBaoTri
        self.soLit = soLit
        self.chiPhi = chiPhi
        self.soKmHienTai = soKmHienTai
        self.ngayBaoTri = ngayBaoTri
        self.ghiChu = ghiChu
    }
}
