//
//  CarModel.swift
//  DemoMobisApplication
//
//  Created by boys vip on 4/14/18.
//  Copyright © 2018 boys vip. All rights reserved.
//

import Foundation

public class CarModel: NSObject {
    var title: String?
    var amount: String?
    var list_ThayLopXe: Array<ThayLopXeModel>?
    var list_BaoDuong: Array<BaoDuongModel>?
    var list_SuaChua: Array<SuaChuaModel>?
    var list_XangDau: Array<XangDauModel>?
    
    init(title: String?, amount: String?, list_ThayLopXe: Array<ThayLopXeModel>?, list_BaoDuong: Array<BaoDuongModel>?, list_SuaChua: Array<SuaChuaModel>?, list_XangDau: Array<XangDauModel>?) {
        self.title = title
        self.amount = amount
        self.list_ThayLopXe = list_ThayLopXe
        self.list_BaoDuong = list_BaoDuong
        self.list_SuaChua = list_SuaChua
        self.list_XangDau = list_XangDau
    }
}
