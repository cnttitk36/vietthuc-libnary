//
//  XangDauModel.swift
//  DemoMobisApplication
//
//  Created by boys vip on 4/14/18.
//  Copyright © 2018 boys vip. All rights reserved.
//

import Foundation

public class XangDauModel: NSObject {
    var sTT: String?
    var soKmHienTai: String?
    var ngayNap: String?
    var soLuong: String?
    var donGia: String?
    var chiPhi: String?
    
    init(sTT: String?, soKmHienTai: String?, ngayNap: String?, soLuong: String?, donGia: String?, chiPhi: String?) {
        self.sTT = sTT
        self.soKmHienTai = soKmHienTai
        self.ngayNap = ngayNap
        self.soLuong = soLuong
        self.donGia = donGia
        self.chiPhi = chiPhi
    }
}
