//
//  XeModel.swift
//  DemoMobisApplication
//
//  Created by boys vip on 4/19/18.
//  Copyright © 2018 boys vip. All rights reserved.
//

import Foundation

public class XeModel: NSObject {
    var title: String?
    var date: String?
    var amount: String?
    var listBaoDuong: SumnaryModel?
    var listXangDau: SumnaryModel?
    var listThayNhot: SumnaryModel?
    var listSuaChua: SumnaryModel?
    var listThayLopXe: SumnaryModel?
    var currentIndex: Int?
    
    init(title: String?, date: String?, amount: String?, listBaoDuong: SumnaryModel?, listXangDau: SumnaryModel?, listThayNhot: SumnaryModel?, listSuaChua: SumnaryModel?, listThayLopXe: SumnaryModel?, currentIndex: Int?) {
        self.title = title
        self.date = date
        self.amount = amount
        self.listBaoDuong = listBaoDuong
        self.listXangDau = listXangDau
        self.listThayNhot = listThayNhot
        self.listSuaChua = listSuaChua
        self.listThayLopXe = listThayLopXe
        self.currentIndex = currentIndex
    }
}
