//
//  HeaderCell.swift
//  DemoMobisApplication
//
//  Created by boys vip on 4/20/18.
//  Copyright © 2018 boys vip. All rights reserved.
//

import UIKit

class HeaderCell: UICollectionViewCell {
    //indexSelected, currentIndex == 0 --> Bảo dưỡng | 1 --> Xăng Dầu | 2 --> Thay Nhớt | 3 --> Sửa Chữa | 4 --> Thay Lốp Xe
    private var _view: FullCollectionView?
    private var _currentSection: Int?
    
    @IBOutlet weak var _title: UILabel!
    
    @IBOutlet weak var _btnBaoDuong: UIButton!
    @IBAction func _btnBaoDuongClick(_ sender: Any) {
        self._view?.updateList(currentSection: self._currentSection!, indexSelected: 0)
    }
    
    @IBOutlet weak var _btnXangDau: UIButton!
    @IBAction func _btnXangDauClick(_ sender: Any) {
        self._view?.updateList(currentSection: self._currentSection!, indexSelected: 1)
    }
    
    @IBOutlet weak var _btnThayNhot: UIButton!
    @IBAction func _btnThayNhotClick(_ sender: Any) {
        self._view?.updateList(currentSection: self._currentSection!, indexSelected: 2)
    }
    
    @IBOutlet weak var _btnSuaChua: UIButton!
    @IBAction func _btnSuaChuaClick(_ sender: Any) {
        self._view?.updateList(currentSection: self._currentSection!, indexSelected: 3)
    }
    
    @IBOutlet weak var _btnThayLopXe: UIButton!
    @IBAction func _btnThayLopXeClick(_ sender: Any) {
        self._view?.updateList(currentSection: self._currentSection!, indexSelected: 4)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func commoInnit(title: String?, view: FullCollectionView, currentSection: Int, indexSelected: Int?) {
        self._title.text = title
        self._view = view
        self._currentSection = currentSection
        if indexSelected == 0 {
            self._btnBaoDuong.backgroundColor = UIColor.yellow
            self._btnXangDau.backgroundColor = UIColor.lightGray
            self._btnThayNhot.backgroundColor = UIColor.lightGray
            self._btnSuaChua.backgroundColor = UIColor.lightGray
            self._btnThayLopXe.backgroundColor = UIColor.lightGray
        }else if indexSelected == 1 {
            self._btnBaoDuong.backgroundColor = UIColor.lightGray
            self._btnXangDau.backgroundColor = UIColor.yellow
            self._btnThayNhot.backgroundColor = UIColor.lightGray
            self._btnSuaChua.backgroundColor = UIColor.lightGray
            self._btnThayLopXe.backgroundColor = UIColor.lightGray
        }else if indexSelected == 2 {
            self._btnBaoDuong.backgroundColor = UIColor.lightGray
            self._btnXangDau.backgroundColor = UIColor.lightGray
            self._btnThayNhot.backgroundColor = UIColor.yellow
            self._btnSuaChua.backgroundColor = UIColor.lightGray
            self._btnThayLopXe.backgroundColor = UIColor.lightGray
        }else if indexSelected == 3 {
            self._btnBaoDuong.backgroundColor = UIColor.lightGray
            self._btnXangDau.backgroundColor = UIColor.lightGray
            self._btnThayNhot.backgroundColor = UIColor.lightGray
            self._btnSuaChua.backgroundColor = UIColor.yellow
            self._btnThayLopXe.backgroundColor = UIColor.lightGray
        }else if indexSelected == 4 {
            self._btnBaoDuong.backgroundColor = UIColor.lightGray
            self._btnXangDau.backgroundColor = UIColor.lightGray
            self._btnThayNhot.backgroundColor = UIColor.lightGray
            self._btnSuaChua.backgroundColor = UIColor.lightGray
            self._btnThayLopXe.backgroundColor = UIColor.yellow
        }
    }
    
}
