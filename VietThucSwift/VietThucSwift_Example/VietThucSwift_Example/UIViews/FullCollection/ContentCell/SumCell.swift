//
//  SumCell.swift
//  DemoMobisApplication
//
//  Created by boys vip on 4/20/18.
//  Copyright © 2018 boys vip. All rights reserved.
//

import UIKit

class SumCell: UICollectionViewCell {

    @IBOutlet weak var _title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupView(_ value: String?) {
        _title.text = value
    }
}
