//
//  FooterCell.swift
//  DemoMobisApplication
//
//  Created by boys vip on 5/15/18.
//  Copyright © 2018 boys vip. All rights reserved.
//

import UIKit

class FooterCell: UICollectionViewCell {
    
    @IBOutlet weak var _txtDate: UILabel!
    @IBOutlet weak var _txtAmount: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func commoInnit(date: String?, amount: String?) {
        self._txtDate.text = date
        self._txtAmount.text = amount
    }
    
}
