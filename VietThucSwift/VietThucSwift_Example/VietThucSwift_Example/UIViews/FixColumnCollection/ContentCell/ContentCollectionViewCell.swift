//
//  ContentCollectionViewCell.swift
//  VietThucSwift
//
//  Created by Trần Việt Thức on 12/31/2018.
//  Copyright (c) 2018 Trần Việt Thức. All rights reserved.
//

import UIKit

class ContentCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var contentLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
