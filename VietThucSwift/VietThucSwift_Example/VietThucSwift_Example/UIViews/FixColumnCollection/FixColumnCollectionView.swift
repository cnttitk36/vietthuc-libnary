//
//  FixColumnCollectionView.swift
//  VietThucSwift_Example
//
//  Created by Tran Viet Thuc on 4/15/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import VietThucSwift

class FixColumnCollectionView: BaseView, UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet weak var _collectionView: UICollectionView!
    
    init(frame: CGRect, rootVC: BaseViewController) {
        super.init(frame: frame)
        self.xibSetup(frame: frame, rootVC: rootVC)
        self.initCollectionView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func willMove(toWindow newWindow: UIWindow?) {
        super.willMove(toWindow: newWindow)
        if newWindow == nil {// UIView disappear
        } else {// UIView appear
        }
    }
    
    //CollectionView
    func initCollectionView() {
        _collectionView.delegate = self
        _collectionView.dataSource = self
        _collectionView.register(UINib(nibName: "ContentCollectionViewCell", bundle: nil),
                                forCellWithReuseIdentifier: "contentCollectionViewCell")
        
        let layout = FixedColumnCollectionViewLayout()
        layout.numberOfColumns = 8
        _collectionView.collectionViewLayout = layout
        _collectionView.isDirectionalLockEnabled = true
        
        _collectionView.layoutIfNeeded()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 50
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "contentCollectionViewCell",
                                                      for: indexPath) as! ContentCollectionViewCell
        
        if indexPath.section % 2 != 0 {
            cell.backgroundColor = UIColor(white: 242/255.0, alpha: 1.0)
        } else {
            cell.backgroundColor = UIColor.white
        }
        
        if indexPath.row % 2 != 0 {
            cell.backgroundColor = UIColor(white: 242/255.0, alpha: 1.0)
        } else {
            cell.backgroundColor = UIColor.white
        }
        
        if indexPath.section == 0 {
            cell.backgroundColor = .green
        }
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                cell.contentLabel.text = "Date0"
            } else if indexPath.row == 1 {
                cell.contentLabel.text = "Date1"
            }else {
                cell.contentLabel.text = "Section\(indexPath.section)"
            }
        } else {
            if indexPath.row == 0 {
                cell.contentLabel.text = String(indexPath.section)
            } else if indexPath.row == 1 {
                cell.contentLabel.text = String(indexPath.section)
            }else {
                cell.contentLabel.text = "S:\(indexPath.section)-R:\(indexPath.row)"
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        Log.debug("Selected: \(indexPath.item)\n")
    }
    //CollectionView ./
}
