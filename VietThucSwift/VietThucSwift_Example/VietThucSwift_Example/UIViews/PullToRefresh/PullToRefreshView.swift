//
//  PullToRefreshView.swift
//  VietThucSwift_Example
//
//  Created by Tran Viet Thuc on 4/20/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import VietThucSwift

class PullToRefreshView: BaseView, TableViewDelegateWidget, UICollectionViewDataSource, UICollectionViewDelegate, UIWebViewDelegate {
    
    init(frame: CGRect, rootVC: BaseViewController) {
        super.init(frame: frame)
        self.xibSetup(frame: frame, rootVC: rootVC)
        self.initTableView()
        self.initTextView()
        self.initCollectionView()
        self.initWebView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func willMove(toWindow newWindow: UIWindow?) {
        super.willMove(toWindow: newWindow)
        if newWindow == nil {// UIView disappear
        } else {// UIView appear
        }
    }
    
    
    @IBOutlet weak var _tableView: TableViewWidget!
    public var page = 1
    public var lenght = 5
    
    //TableView
    private var listData = Array<String>()
    func initTableView() {
        for i in 0...self.lenght {
            listData.append("Cell \(i)...")
        }
        
        _tableView.initRegistry(["mainCell"], [], 60.0, self)
        _tableView.estimatedRowHeight = 60
        _tableView.rowHeight = UITableView.automaticDimension
        _tableView.separatorStyle = .none
        _tableView.separatorColor = UIColor.clear
        
        var header: ESRefreshProtocol & ESRefreshAnimatorProtocol
        var footer: ESRefreshProtocol & ESRefreshAnimatorProtocol
        //Style-day
        //header = ESRefreshDayHeaderAnimator.init(frame: CGRect.zero)
        //footer = ESRefreshFooterAnimator.init(frame: CGRect.zero)
        
        //Style-defaulttype
        header = ESRefreshHeaderAnimator(frame: CGRect.zero)
        footer = ESRefreshFooterAnimator(frame: CGRect.zero)
        
        //Style-meituan
        //header = MTRefreshHeaderAnimator(frame: CGRect.zero)
        //footer = MTRefreshFooterAnimator(frame: CGRect.zero)
        
        //Style-wechat
        //header = WCRefreshHeaderAnimator(frame: CGRect.zero)
        //footer = ESRefreshFooterAnimator(frame: CGRect.zero)
        
        (header as? ESRefreshHeaderAnimator)?.pullToRefreshDescription = "Kéo thả để cập nhật...";
        (header as? ESRefreshHeaderAnimator)?.releaseToRefreshDescription = "Thả ra để cập nhật...";
        (header as? ESRefreshHeaderAnimator)?.loadingDescription = "Đang tải dữ liệu...";
        
        (footer as? ESRefreshFooterAnimator)?.loadingMoreDescription = "Tải thêm dữ liệu..."
        (footer as? ESRefreshFooterAnimator)?.noMoreDataDescription = "Không còn dữ liệu mới"
        (footer as? ESRefreshFooterAnimator)?.loadingDescription = "Đang tải thêm dữ liệu..."
        
        // HeaderView like WeChat
        let headerView = WeChatTableHeaderView(frame: CGRect(origin: CGPoint.zero,
                                                             size: CGSize(width: self.bounds.size.width,
                                                                          height: 100)))
        _tableView.tableHeaderView = headerView
        
        _tableView.es.addPullToRefresh(animator: header) { [weak self] in
            self?.refresh()
        }
        _tableView.es.addInfiniteScrolling(animator: footer) { [weak self] in
            self?.loadMore()
        }
        _tableView.refreshIdentifier = String(describing: "defaulttype")
        _tableView.expiredTimeInterval = 20.0
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self._tableView.es.autoPullToRefresh()
        }
    }
    
    private func refresh() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            self.page = 1
            self.listData.removeAll()
            for i in 0..<(self.page*self.lenght) {
                self.listData.append("Cell \(i)...")
            }
            self._tableView.reloadData()
            self._tableView.es.stopPullToRefresh()
        }
    }
    
    private func loadMore() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            self.page += 1
            if self.page <= 3 {
                for i in ((self.page-1)*self.lenght)..<(self.page*self.lenght) {
                    self.listData.append("Cell \(i)...")
                }
                self._tableView.reloadData()
                self._tableView.es.stopLoadingMore()
            } else {
                self._tableView.es.noticeNoMoreData()
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int? {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int? {
        return self.listData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat? {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat? {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, nibForRowAt indexPath: IndexPath) -> String? {
        return "mainCell"
    }
    
    func tableView(_ tableView: UITableView, _ row: UITableViewCell, cellForRowAt indexPath: IndexPath) -> UITableViewCell? {
        if indexPath.row % 2 != 0 {
            row.backgroundColor = UIColor(white: 242/255.0, alpha: 1.0)
        } else {
            row.backgroundColor = UIColor.white
        }
        
        (row as? MainCell)?.commonInit(self.listData[indexPath.row])
        row.accessoryType = .disclosureIndicator
        row.selectionStyle = .gray
        return row
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Log.debug("Selected: \(indexPath.row)\n")
    }
    //TableView ./
    
    
    
    @IBOutlet weak var _textView: UITextView!
    var num: Int = 0
    var text1: String = "Sáng 20/4, một lãnh đạo UBND quận Bắc Từ Liêm xác nhận: Ông Nguyễn Văn Sỹ - Chánh văn phòng quận Bắc Từ Liêm tử vong tại nhà riêng tại phường Phúc Diễn vào trưa hôm qua (19/4)."
    var text2: String = "\n\nCùng trong chiều 19/4, gia đình tổ chức lễ viếng cho ông Nguyễn Văn Sỹ. Lễ viếng diễn ra trong 2 ngày, chiều 19/4 và sáng 20/4."
    var text3: String = "\n\nVới một số thông tin nói về việc ông Sỹ tử vong, vị này cho biết, ông Sỹ bị cảm khi đi viện về thì đột ngột qua đời."
    var text4: String = "\n\nTrước khi làm Chánh văn phòng UBND quận Bắc từ Liêm, ông Nguyễn Văn Sỹ từng đảm nhiệm chức Chủ tịch UBND xã Minh Khai."
    var text5: String = "\n\nChương trình được đánh giá là một động thái tích cực dựa trên sự thấu hiểu tâm lý người tiêu dùng. Theo số liệu thống kê tính đến cuối tháng 12/2018 của Ngân Hàng Nhà Nước."
    var text6: String = "\n\nNgoài lợi ích đem đến cho khách hàng, chương trình cũng giúp tạo ra lợi thế cạnh tranh mạnh mẽ trên thị trường ngành ngân hàng, đặc biệt là ở mảng tài khoản thanh toán."
    var text7: String = "\n\nTrong thời gian qua, VIB cũng tạo được ấn tượng với nền tảng ngân hàng số giúp giao dịch nhanh chóng, thuận tiện và an toàn. Đây là yếu tố quyết định chọn tài khoản thanh toán và gắn bó lâu dài của khách hàng với ngân hàng."
    
    func initTextView() {
        _textView.isEditable = false
        _textView.alwaysBounceVertical = true
        _textView.textColor = UIColor.init(white: 0.3, alpha: 1.0)
        _textView.textAlignment = .justified
        _textView.textContainerInset = UIEdgeInsets.init(top: 12, left: 8, bottom: 12, right: 8)
        
        _textView.es.addPullToRefresh {
            [weak self] in
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self?.num = 0
                let style = NSMutableParagraphStyle.init()
                style.lineSpacing = 0.0
                style.firstLineHeadIndent = 10.0
                style.alignment = .justified
                self?._textView.attributedText = NSAttributedString.init(string: (self?.text1)!, attributes: [NSAttributedString.Key.paragraphStyle : style, NSAttributedString.Key.font: UIFont.init(name: "Arial", size: 16.0)!, NSAttributedString.Key.foregroundColor: UIColor.init(white: 0.3, alpha: 1.0)])
                self?._textView.es.stopPullToRefresh()
            }
        }
        
        _textView.es.addInfiniteScrolling {
            [weak self] in
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                var i = self?.num ?? 0
                i += 1
                self?.num = i
                
                var str: String = self?.text1 ?? ""
                if i >= 1 {
                    str += self?.text2 ?? ""
                }
                if i >= 2 {
                    str += self?.text3 ?? ""
                }
                if i >= 3 {
                    str += self?.text4 ?? ""
                }
                if i >= 4 {
                    str += self?.text5 ?? ""
                }
                if i >= 5 {
                    str += self?.text6 ?? ""
                }
                if i >= 6 {
                    str += self?.text7 ?? ""
                }
                if i >= 7 {
                    self?._textView.es.noticeNoMoreData()
                } else {
                    let style = NSMutableParagraphStyle.init()
                    style.lineSpacing = 0.0
                    style.firstLineHeadIndent = 10.0
                    style.alignment = .justified
                    self?._textView.attributedText = NSAttributedString.init(string: str, attributes: [NSAttributedString.Key.paragraphStyle : style, NSAttributedString.Key.font: UIFont.init(name: "Arial", size: 16.0)!, NSAttributedString.Key.foregroundColor: UIColor.init(white: 0.3, alpha: 1.0)])
                    
                    self?._textView.es.stopLoadingMore()
                }
            }
        }
        
        _textView.es.startPullToRefresh()
    }
    
    
    @IBOutlet weak var _collectionView: UICollectionView!
    private var list: [String] = []
    //CollectionView
    func initCollectionView() {
        _collectionView.delegate = self
        _collectionView.dataSource = self
        _collectionView.register(UINib(nibName: "ContentCollectionViewCell", bundle: nil),
                                 forCellWithReuseIdentifier: "contentCollectionViewCell")
        
        let layout = _collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let itemWidth = (UIScreen.main.bounds.width - 40) / 3
        layout.itemSize = CGSize(width: itemWidth, height: itemWidth)
        layout.minimumLineSpacing = 5.0
        layout.minimumInteritemSpacing = 5.0
        layout.scrollDirection = .vertical
        
        _collectionView.es.addPullToRefresh {
            self.delay(time: 2) {
                self.list = []
                for item in 0...20 {
                    self.list.append(String(item))
                }
                self._collectionView.es.stopPullToRefresh()
                self._collectionView.reloadData()
            }
        }
        
        _collectionView.es.addInfiniteScrolling {
            self.delay(time: 2) {
                for item in 21...40 {
                    self.list.append(String(item))
                }
                self._collectionView.es.stopLoadingMore()
                self._collectionView.reloadData()
            }
        }
        
        _collectionView.es.startPullToRefresh()
    }
    
    func delay(time: TimeInterval, completionHandler: @escaping ()-> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + time) {
            completionHandler()
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "contentCollectionViewCell",
                                                      for: indexPath) as! ContentCollectionViewCell
        
        if indexPath.row % 2 != 0 {
            cell.backgroundColor = UIColor(white: 242/255.0, alpha: 1.0)
        } else {
            cell.backgroundColor = UIColor.white
        }
        
        cell.contentLabel.text = self.list[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        Log.debug("Selected: \(indexPath.item)\n")
    }
    //CollectionView ./
    
    
    
    @IBOutlet weak var _webView: UIWebView!
    
    func initWebView() {
        _webView!.delegate = self
        
        let url = "https://gitlab.com/cnttitk36/vietthuc-libnary"
        let request = NSURLRequest(url: NSURL(string: url)! as URL)
        
        _webView.scrollView.es.addPullToRefresh {
            [weak self] in
            self?._webView.loadRequest(request as URLRequest)
        }
        
        _webView.scrollView.es.startPullToRefresh()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        _webView.scrollView.es.stopPullToRefresh()
        _webView.scrollView.bounces = true
        _webView.scrollView.alwaysBounceVertical = true
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        _webView.scrollView.es.stopPullToRefresh(ignoreDate: true)
    }
}
