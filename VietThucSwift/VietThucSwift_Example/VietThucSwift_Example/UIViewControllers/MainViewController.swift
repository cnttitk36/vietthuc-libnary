//
//  ViewController.swift
//  VietThucSwift
//
//  Created by Trần Việt Thức on 12/31/2018.
//  Copyright (c) 2018 Trần Việt Thức. All rights reserved.
//

import UIKit
import VietThucSwift

class MainViewController: BaseViewController, TableViewDelegateWidget {
    
    @IBOutlet weak var _baseScroll: UIScrollView!
    @IBOutlet weak var _baseView: UIView!
    @IBOutlet weak var _tableView: TableViewWidget!
    
    override func viewDidLoad() {
        super.viewDidLoad(false, true, false, false)
        //Add constraint Safe Area
        self.addConstraint(newView: self.rootScrollView(), rootView: self.view,
                           showStatusBar: true, fullScreen: false, fullScreenX: false)
        self.addConstraint(newView: self.rootView(), rootView: self.view,
                           showStatusBar: true, fullScreen: false, fullScreenX: false)
        //self.addConstraintOnScrollView(newView: self.rootView(), view: self.rootScrollView())
        
        self.initTableView()
        _tableView.reloadData()
    }
    
    override func rootScrollView() -> UIScrollView {
        return self._baseScroll
    }
    
    override func rootView() -> UIView {
        return self._baseView
    }
    
    //TableView
    private var listData = Array<String>()
    func initTableView() {
        listData.append("Demo CollectionView cố định 2 cột và 1 dòng đầu tiên...")
        listData.append("Demo CollectionView hiển thị toàn bộ khung nhìn...")
        listData.append("Demo hiển thị định dạng HTML...")
        listData.append("Demo Pull to Refresh UITableView, UICollectionView, UIScrollView, UIWebView...")
        listData.append("Demo Lưu cache image, string, json, video, data...")
        listData.append("Demo FoldingCell...")
        
        _tableView.initRegistry(["mainCell"], [], 60.0, self)
        _tableView.estimatedRowHeight = 60
        _tableView.rowHeight = UITableView.automaticDimension
        _tableView.separatorStyle = .none
    }
    
    func numberOfSections(in tableView: UITableView) -> Int? {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int? {
        return self.listData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat? {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat? {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, nibForRowAt indexPath: IndexPath) -> String? {
        return "mainCell"
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        // Animation Cell hiện ra đậm hơn
        let animation = AnimationFactory.makeFadeAnimation(duration: 0.5, delayFactor: 0.05)
        let animator = Animator(animation: animation)
        animator.animate(cell: cell, at: indexPath, in: tableView)
    }
    
    func tableView(_ tableView: UITableView, _ row: UITableViewCell, cellForRowAt indexPath: IndexPath) -> UITableViewCell? {
        if indexPath.row % 2 != 0 {
            row.backgroundColor = UIColor(white: 242/255.0, alpha: 1.0)
        } else {
            row.backgroundColor = UIColor.white
        }
        
        (row as? MainCell)?.commonInit(self.listData[indexPath.row])
        row.accessoryType = .disclosureIndicator
        row.selectionStyle = .gray
        return row
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Log.debug("Selected: \(indexPath.row)\n")
        switch indexPath.row {
        case 0:
            let fixColumnCollectionVC = FixColumnCollectionVC()
            self.navigationController?.pushViewController(fixColumnCollectionVC, animated: true)
            break
        case 1:
            let fullCollectionVC = FullCollectionVC()
            self.navigationController?.pushViewController(fullCollectionVC, animated: true)
            break
        case 2:
            let atributikaVC = AtributikaVC()
            self.navigationController?.pushViewController(atributikaVC, animated: true)
            break
        case 3:
            let pullToRefreshVC = PullToRefreshVC()
            self.navigationController?.pushViewController(pullToRefreshVC, animated: true)
            break
        case 4:
            let hanekeVC = HanekeVC()
            self.navigationController?.pushViewController(hanekeVC, animated: true)
            break
        case 5:
            let foldingCellVC = FoldingCellVC()
            self.navigationController?.pushViewController(foldingCellVC, animated: true)
            break
        case 6:
            break
        case 7:
            break
        case 8:
            break
        case 9:
            break
        case 10:
            break
        default:
            break
        }
    }
    //TableView ./
}

