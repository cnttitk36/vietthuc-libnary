//
//  FixColumnCollectionVC.swift
//  VietThucSwift_Example
//
//  Created by Tran Viet Thuc on 4/15/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit

class FixColumnCollectionVC: BaseViewController {

    @IBOutlet weak var _baseScroll: UIScrollView!
    @IBOutlet weak var _baseView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad(true, true)
        
        let fixColumnView = FixColumnCollectionView(frame: self.frameIphone!, rootVC: self)
        pushView(fixColumnView)
    }
    
    override func rootScrollView() -> UIScrollView {
        return self._baseScroll
    }
    
    override func rootView() -> UIView {
        return self._baseView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
}
