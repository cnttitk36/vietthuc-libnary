//
//  DemoCell.swift
//  VietThucSwift_Example
//
//  Created by TranVietThuc on 7/31/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import VietThucSwift

// add Constraint ForegroundViewTop: Left, Top, Right
// add Constraint ContainerViewTop: Left, Top, Right
class DemoCell: FoldingCell {
    
    @IBOutlet var closeNumberLabel: UILabel!
    @IBOutlet var openNumberLabel: UILabel!
    @IBAction func buttonHandler(_: AnyObject) {
        Log.debug("tap")
    }
    
    var number: Int = 0 {
        didSet {
            closeNumberLabel.text = String(number)
            openNumberLabel.text = String(number)
        }
    }
    
    override func awakeFromNib() {
        foregroundView.layer.cornerRadius = 10
        foregroundView.layer.masksToBounds = true
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func animationDuration(_ itemIndex: NSInteger, type _: FoldingCell.AnimationType) -> TimeInterval {
        let durations = [0.26, 0.2, 0.2]
        return durations[itemIndex]
    }
}
