//
//  FoldingCellVC.swift
//  VietThucSwift_Example
//
//  Created by TranVietThuc on 7/31/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import VietThucSwift

class FoldingCellVC: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var _tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    enum Const {
        static let closeCellHeight: CGFloat = 179 // equal or greater foregroundView height
        static let openCellHeight: CGFloat = 488 // equal or greater containerView height
        static let rowsCount = 10
    }
    var cellHeights: [CGFloat] = []
    
    // MARK: Helpers
    private func setup() {
        cellHeights = Array(repeating: Const.closeCellHeight, count: Const.rowsCount)
        _tableView.delegate = self
        _tableView.dataSource = self
        _tableView.estimatedRowHeight = Const.closeCellHeight
        _tableView.rowHeight = UITableView.automaticDimension
        _tableView.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "background"))
        _tableView.register(UINib(nibName: "DemoCell", bundle: nil), forCellReuseIdentifier: "demoCell")
        if #available(iOS 10.0, *) {
            _tableView.refreshControl = UIRefreshControl()
            _tableView.refreshControl?.addTarget(self, action: #selector(refreshHandler), for: .valueChanged)
        }
        _tableView.reloadData()
    }
    
    // MARK: Actions
    @objc func refreshHandler() {
        let deadlineTime = DispatchTime.now() + .seconds(1)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime, execute: { [weak self] in
            if #available(iOS 10.0, *) {
                self?._tableView.refreshControl?.endRefreshing()
            }
            self?._tableView.reloadData()
        })
    }
    
    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return Const.rowsCount
    }
    
    func tableView(_: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard case let cell as DemoCell = cell else {
            return
        }
        if cellHeights[indexPath.row] == Const.closeCellHeight {
            cell.unfold(false, animated: false, completion: nil)
        } else {
            cell.unfold(true, animated: false, completion: nil)
        }
        cell.number = indexPath.row
        cell.backgroundColor = .clear
    }
    
    func tableView(_: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "demoCell", for: indexPath) as! FoldingCell
        let durations: [TimeInterval] = [0.26, 0.2, 0.2]
        cell.durationsForExpandedState = durations
        cell.durationsForCollapsedState = durations
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! FoldingCell
        self.openOrCloseCell(cell: cell, tableView: tableView, indexPath: indexPath)
    }
    
    func openOrCloseCell(cell: FoldingCell, tableView: UITableView, indexPath: IndexPath) {
        if cell.isAnimating() {
            return
        }
        
        var duration = 0.0
        let cellIsCollapsed = cellHeights[indexPath.row] == Const.closeCellHeight
        if cellIsCollapsed {
            Log.debug("Open Cell")
            cellHeights[indexPath.row] = Const.openCellHeight
            cell.unfold(true, animated: true, completion: nil)
            duration = 0.5
        } else {
            Log.debug("Close Cell")
            cellHeights[indexPath.row] = Const.closeCellHeight
            cell.unfold(false, animated: true, completion: nil)
            duration = 0.8
        }
        
        UIView.animate(withDuration: duration, delay: 0, options: .curveEaseOut, animations: { () -> Void in
            tableView.beginUpdates()
            tableView.endUpdates()
            
            if cell.frame.maxY > tableView.frame.maxY {
                tableView.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
            }
        }, completion: nil)
    }
}
