//
//  MainCell.swift
//
//  Created by boys vip on 12/24/18.
//  Copyright © 2018 boys vip. All rights reserved.
//

import UIKit

class MainCell: UITableViewCell {

    @IBOutlet weak var _viewShadow: UIView!
    @IBOutlet weak var _lbTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func commonInit(_ value: String?) {
        _lbTitle.text = value
    }
}
