//
//  AtributikaVC.swift
//  VietThucSwift_Example
//
//  Created by Tran Viet Thuc on 4/18/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import VietThucSwift

class AtributikaVC: BaseViewController {

    @IBOutlet weak var _baseScroll: UIScrollView!
    @IBOutlet weak var _baseView: UIView!
    
    @IBOutlet weak var _lb1: UILabel!
    @IBOutlet weak var _lb2: UILabel!
    @IBOutlet weak var _lb3: UILabel!
    @IBOutlet weak var _lb4: UILabel!
    @IBOutlet weak var _lb5: UILabel!
    @IBOutlet weak var _lb6: UILabel!
    @IBOutlet weak var _lb7: UILabel!
    @IBOutlet weak var _lb8: UILabel!
    @IBOutlet weak var _lb9: UILabel!
    @IBOutlet weak var _lb10: UILabel!
    @IBOutlet weak var _lb11: UILabel!
    @IBOutlet weak var _lb12: UILabel!
    @IBOutlet weak var _lb13: UILabel!
    @IBOutlet weak var _lb14: UILabel!
    @IBOutlet weak var _lb15: UILabel!
    @IBOutlet weak var _lb16: UILabel!
    @IBOutlet weak var _tweetLabel: AttributedLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad(false, true)
        self.initData()
        self.initEvent()
    }
    
    override func rootScrollView() -> UIScrollView {
        return self._baseScroll
    }
    
    override func rootView() -> UIView {
        return self._baseView
    }
    
    func initData() {
        _lb1.attributedText = self.allSnippets()[0]
        _lb1.numberOfLines = 0
        _lb2.attributedText = self.allSnippets()[1]
        _lb2.numberOfLines = 0
        _lb3.attributedText = self.allSnippets()[2]
        _lb3.numberOfLines = 0
        _lb4.attributedText = self.allSnippets()[3]
        _lb4.numberOfLines = 0
        _lb5.attributedText = self.allSnippets()[4]
        _lb5.numberOfLines = 0
        _lb6.attributedText = self.allSnippets()[5]
        _lb6.numberOfLines = 0
        _lb7.attributedText = self.allSnippets()[6]
        _lb7.numberOfLines = 0
        _lb8.attributedText = self.allSnippets()[7]
        _lb8.numberOfLines = 0
        _lb9.attributedText = self.allSnippets()[8]
        _lb9.numberOfLines = 0
        _lb10.attributedText = self.allSnippets()[9]
        _lb10.numberOfLines = 0
        _lb11.attributedText = self.allSnippets()[10]
        _lb11.numberOfLines = 0
        _lb12.attributedText = self.allSnippets()[11]
        _lb12.numberOfLines = 0
        _lb13.attributedText = self.allSnippets()[12]
        _lb13.numberOfLines = 0
        _lb14.attributedText = self.allSnippets()[13]
        _lb14.numberOfLines = 0
        _lb15.attributedText = self.allSnippets()[14]
        _lb15.numberOfLines = 0
        _lb16.attributedText = self.allSnippets()[15]
        _lb16.numberOfLines = 0
    }
    
    func initEvent() {
        let style = NSMutableParagraphStyle()
        style.alignment = NSTextAlignment.center
        let all = Style.font(.systemFont(ofSize: 14)).custom(style, forAttributedKey: .paragraphStyle)
        let link = Style("a")
            .foregroundColor(.blue, .normal)
            .foregroundColor(.brown, .highlighted)
        
        _tweetLabel.numberOfLines = 0
        _tweetLabel.attributedText = "If only Bradley's @e2F arm was longer. Check this <a href=\"https://facebook.com\">linkFB</a>. If only Bradley's #Mobiapps for a follow 👽. If follow only Bradley's If only Bradley's 😊 #tweetbowls"
            .style(tags: link)
            .styleHashtags(link)
            .styleMentions(link)
            .styleLinks(link)
            .styleAll(all)

        _tweetLabel.onClick = { label, detection in
            switch detection.type {
            case .hashtag(let tag):
                if let url = URL(string: "https://twitter.com/hashtag/\(tag)") {
                    UIApplication.shared.openURL(url)
                }
            case .mention(let name):
                if let url = URL(string: "https://google.com/\(name)") {
                    UIApplication.shared.openURL(url)
                }
            case .link(let url):
                UIApplication.shared.openURL(url)
            case .tag(let tag):
                if tag.name == "a", let href = tag.attributes["href"], let url = URL(string: href) {
                    UIApplication.shared.openURL(url)
                }
            default:
                break
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    
    
    
    func allSnippets() -> [NSAttributedString] {
        return [
            stringWithAtributikaLogo(),
            stringWithTagsAndEmoji(),
            stringWithHashTagAndMention(),
            stringWithPhone(),
            stringWithLink(),
            stringWithManyDetectables(),
            stringWith3Tags(),
            stringWithGrams(),
            stringWithStrong(),
            stringWithTagAndHashtag(),
            stringWithUnorderedList(),
            stringWithHref(),
            stringWithBoldItalicUnderline(),
            stringWithImage(),
            stringWithStrikethrough(),
            stringWithColors()
        ]
    }
    
    func stringWithAtributikaLogo() -> NSAttributedString {
        
        let redColor = UIColor(red:(0xD0 / 255.0), green: (0x02 / 255.0), blue:(0x1B / 255.0), alpha:1.0)
        let a = Style("a").foregroundColor(redColor)
        
        let font = UIFont(name: "AvenirNext-Regular", size: 24)!
        let grayColor = UIColor(white: 0x66 / 255.0, alpha: 1)
        let all = Style.font(font).foregroundColor(grayColor)
        
        let str = "<a>&lt;a&gt;</a>tributik<a>&lt;/a&gt;</a>".style(tags: a)
            .styleAll(all)
            .attributedString
        return str
    }
    
    func stringWithTagsAndEmoji() -> NSAttributedString {
        let b = Style("b").font(.boldSystemFont(ofSize: 20)).foregroundColor(.red)
        let all = Style.font(.systemFont(ofSize: 20))
        let str = "Hello <b>W🌎rld</b>!!!".style(tags: b)
            .styleAll(all)
            .attributedString
        return str
    }
    
    func stringWithHashTagAndMention() -> NSAttributedString {
        
        let str = "#Hello @World!!!"
            .styleHashtags(Style.font(.boldSystemFont(ofSize: 45)))
            .styleMentions(Style.foregroundColor(.red))
            .attributedString
        return str
    }
    
    func stringWithPhone() -> NSAttributedString {
        let str = "Call me (888)555-5512"
            .stylePhoneNumbers(Style.foregroundColor(.red))
            .attributedString
        return str
    }
    
    func stringWithLink() -> NSAttributedString {
        let str = "Check this http://google.com"
            .styleLinks(Style.foregroundColor(.blue))
            .attributedString
        return str
    }
    
    func stringWithManyDetectables() -> NSAttributedString {
        
        let links = Style.foregroundColor(.blue)
        let phoneNumbers = Style.backgroundColor(.yellow)
        let mentions = Style.font(.italicSystemFont(ofSize: 12)).foregroundColor(.black)
        let b = Style("b").font(.boldSystemFont(ofSize: 12))
        
        #if swift(>=4.2)
        let u = Style("u").underlineStyle(.single)
        #else
        let u = Style("u").underlineStyle(.styleSingle)
        #endif
        
        let all = Style.font(.systemFont(ofSize: 12)).foregroundColor(.gray)
        
        let str = "@all I found <u>really</u> nice framework to manage attributed strings. It is called <b>Atributika</b>. Call me if you want to know more (123)456-7890 #swift #nsattributedstring https://github.com/psharanda/Atributika"
            .style(tags: u, b)
            .styleMentions(mentions)
            .styleHashtags(links)
            .styleLinks(links)
            .stylePhoneNumbers(phoneNumbers)
            .styleAll(all)
            .attributedString
        
        return str
    }
    
    func stringWith3Tags() -> NSAttributedString {
        let str = "<r>first</r><g>sec⚽️nd</g><b>third</b>"
            .style(tags: Style("r").foregroundColor(.red),
                   Style("g").foregroundColor(.green),
                   Style("b").foregroundColor(.blue)
            ).attributedString
        return str
    }
    
    func stringWithGrams() -> NSAttributedString {
        let calculatedCoffee: Int = 768
        let g = Style("g").font(.boldSystemFont(ofSize: 12)).foregroundColor(.red)
        let all = Style.font(.systemFont(ofSize: 12))
        
        let str = "\(calculatedCoffee)<g>g</g>".style(tags: g)
            .styleAll(all)
            .attributedString
        
        return str
    }
    
    func stringWithStrong() -> NSAttributedString {
        let str = "<strong>Nice</strong> try, Phil".style(tags:
            Style("strong").font(.boldSystemFont(ofSize: 15)))
            .attributedString
        return str
    }
    
    func stringWithTagAndHashtag() -> NSAttributedString {
        let str = "<b>Hello</b> #World"
        let data = str.data(using: .utf8)
        let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html,NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue]
        
        let htmlAttrString = try! NSAttributedString(data: data!, options: options, documentAttributes: nil)
        let result = htmlAttrString.styleHashtags(Style.foregroundColor(.blue)).attributedString
        return result
    }
    
    func stringWithUnorderedList() -> NSAttributedString {
        let transformers: [TagTransformer] = [
            TagTransformer.brTransformer,
            TagTransformer(tagName: "li", tagType: .start, replaceValue: "- "),
            TagTransformer(tagName: "li", tagType: .end, replaceValue: "\n")
        ]
        
        let li = Style("li").font(.systemFont(ofSize: 12)).foregroundColor(.red)
        
        return "TODO:<br><li>veni</li><li>vidi</li><li>vici</li>"
            .style(tags: li, transformers: transformers)
            .styleAll(Style.font(.boldSystemFont(ofSize: 14)))
            .attributedString
    }
    
    func stringWithHref() -> NSAttributedString {
        return "Hey\r\n<a style=\"text-decoration:none\" href=\"http://www.google.com\">Hello\r\nWorld</a>s".style(tags:
            Style("a").font(.boldSystemFont(ofSize: 45)).foregroundColor(.red)
            ).attributedString
    }
    
    func stringWithBoldItalicUnderline() -> NSAttributedString {
        let font = UIFont(name: "HelveticaNeue-BoldItalic", size: 12)!
        #if swift(>=4.2)
        let uib = Style("uib").font(font).underlineStyle(.single)
        #else
        let uib = Style("uib").font(font).underlineStyle(.styleSingle)
        #endif
        
        let str = "<br><uib>Italicunderline</uib>".style(tags: uib)
            .attributedString
        return str
    }
    
    func stringWithImage() -> NSAttributedString {
        let font = UIFont(name: "HelveticaNeue-BoldItalic", size: 12)!
        
        #if swift(>=4.2)
        let uib = Style("b").font(font).underlineStyle(.single)
        #else
        let uib = Style("b").font(font).underlineStyle(.styleSingle)
        #endif
        let str = "<b>Running</b> with <img id=\"scissors\"></img>!".style(tags: uib)
        
        let mutableAttrStr = NSMutableAttributedString(attributedString: str.attributedString)
        
        var locationShift = 0
        for detection in str.detections {
            switch detection.type {
            case .tag(let tag):
                if let imageId =  tag.attributes["id"] {
                    let textAttachment = NSTextAttachment()
                    textAttachment.image = UIImage(named: imageId)
                    let imageAttrStr = NSAttributedString(attachment: textAttachment)
                    let nsrange = NSRange.init(detection.range, in: mutableAttrStr.string)
                    mutableAttrStr.insert(imageAttrStr, at: nsrange.location + locationShift)
                    locationShift += 1
                }
            default:
                break
            }
        }
        
        return mutableAttrStr
    }
    
    func stringWithStrikethrough() -> NSAttributedString {
        let all = Style.font(.systemFont(ofSize: 20))
        #if swift(>=4.2)
        let strike = Style("strike").strikethroughStyle(.single).strikethroughColor(.black)
        #else
        let strike = Style("strike").strikethroughStyle(.styleSingle).strikethroughColor(.black)
        #endif
        
        let code = Style("code").foregroundColor(.red)
        
        let str = "<code>my code</code> <strike>test</strike> testing"
            .style(tags: [strike,code])
            .styleAll(all)
            .attributedString
        return str
    }
    
    func stringWithColors() -> NSAttributedString {
        let r = Style("r").foregroundColor(.red)
        let g = Style("g").foregroundColor(.green)
        let b = Style("b").foregroundColor(.blue)
        let c = Style("c").foregroundColor(.cyan)
        let m = Style("m").foregroundColor(.magenta)
        let y = Style("y").foregroundColor(.yellow)
        
        let str = "<r>Hello <g>w<c>orld<m></g>; he<y>l</y></c>lo atributika</r></m>".style(tags: r, g, b, c, m, y)
            .attributedString
        return str
    }
}
