//
//  BaseViewController.swift
//  TemplaceProject
//
//  Created by boys vip on 5/29/18.
//  Copyright © 2018 boys vip. All rights reserved.
//

import UIKit
import VietThucSwift

class BaseViewController: RootViewController {
    
    override func viewDidLoad(_ removeView: Bool = true,_ showStatusBar: Bool = true,_ fullScreen: Bool = true,_ fullScreenX: Bool = false) {
        super.autoConstrain = true
        super.viewDidLoad(removeView, showStatusBar)
        Log.debug("Frame without Constrain: \(self.frameIphone?.debugDescription ?? "")\n")
    }
}
