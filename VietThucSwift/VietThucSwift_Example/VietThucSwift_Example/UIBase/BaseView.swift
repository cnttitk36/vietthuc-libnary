//
//  BaseView.swift
//  TemplaceProject
//
//  Created by boys vip on 5/29/18.
//  Copyright © 2018 boys vip. All rights reserved.
//

import UIKit
import VietThucSwift

class BaseView: RootView {
    
    override func xibSetup(frame: CGRect, rootVC: RootViewController?) {
        super.xibSetup(frame: frame, rootVC: rootVC)
    }
    
}
