# VietThucSwift

[![CI Status](https://img.shields.io/travis/Trần Việt Thức/VietThucSwift.svg?style=flat)](https://travis-ci.org/Trần Việt Thức/VietThucSwift)
[![Version](https://img.shields.io/cocoapods/v/VietThucSwift.svg?style=flat)](https://cocoapods.org/pods/VietThucSwift)
[![License](https://img.shields.io/cocoapods/l/VietThucSwift.svg?style=flat)](https://cocoapods.org/pods/VietThucSwift)
[![Platform](https://img.shields.io/cocoapods/p/VietThucSwift.svg?style=flat)](https://cocoapods.org/pods/VietThucSwift)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

VietThucSwift is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'VietThucSwift'
```

## Author

Trần Việt Thức, cnttitk36@gmail.com

## License

VietThucSwift is available under the MIT license. See the LICENSE file for more info.
