//
//  RootViewController.swift
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

open class RootViewController: UIViewController, UIScrollViewDelegate {
    public var autoConstrain: Bool = false
    public var frameIphone: CGRect?
    public var frameFocus: UIView?
    public var tabbarHeight: CGFloat = 0.0
    
    public var stack: NSMutableArray = []
    public var cache: NSMutableArray = []
    
    open func rootScrollView() -> UIScrollView {
        return UIScrollView(frame: self.view.frame)
    }
    
    open func rootView() -> UIView {
        return self.view
    }
    
    // Push-Pop View
    open func pushView(_ object : UIView, isRemove: Bool = true) {
        self.stack.insert(object, at: 0)
        if isRemove {
            self.clearAllSubView(self.rootView())
        }
        self.rootView().addSubview(object)
        if self.autoConstrain {
            self.addConstraint(newView: object, rootView: self.rootView())
        }
    }
    
    open func popView(isSave: Bool = false) {
        if self.stack.count > 0 {
            if isSave {
                self.cache.insert(stack[0], at: 0)
            }
            self.stack.removeObject(at: 0)
            //get top object
            if self.stack.count > 0 {
                let topObject = stack[0] as! UIView
                self.clearAllSubView(self.rootView())
                self.rootView().addSubview(topObject)
                if self.autoConstrain {
                    self.addConstraint(newView: topObject, rootView: self.rootView())
                }
            } else {
                self.navigationController?.popViewController(animated: true)
            }
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    open func popView(_ toIndex : Int, isSave: Bool = false) {
        while toIndex < self.stack.count - 1 {
            self.popView(isSave: isSave)
        }
    }
    
    open func popView<T>(myType: T.Type, isSave: Bool = false) {
        var isExitsView = false
        var toIndex = 0
        for i in 0..<self.stack.count {
            if self.stack[i] is T {
                isExitsView = true
                toIndex = self.stack.count - i - 1
                break
            }
        }
        if isExitsView {
            self.popView(toIndex, isSave: isSave)
        }
    }
    
    open func getViewStack(_ toIndex : Int) -> UIView? {
        if toIndex < stack.count {
            let topObject = self.stack[toIndex] as? UIView
            return topObject
        }
        return self.rootView()
    }
    
    open func getViewCache(_ toIndex : Int) -> UIView? {
        if toIndex < self.cache.count {
            let topObject = self.cache[toIndex] as? UIView
            return topObject
        }
        return nil
    }
    
    open func clearAllSubView(_ view: UIView) {
        for item in view.subviews{
            item.removeFromSuperview()
        }
    }
    
    open func removeViewWithTag(_ view: UIView ,_ tag: Int) {
        view.viewWithTag(tag)?.removeFromSuperview()
    }
    // Push-Pop View ./
    
    
    //Status bar
    //<key>UIStatusBarHidden</key>
    //<true/>
    //<key>UIViewControllerBasedStatusBarAppearance</key>
    //<true/>
    open var statusBarHidden = false {
        didSet {
            UIView.animate(withDuration: 0.3) { () -> Void in
                self.setNeedsStatusBarAppearanceUpdate()
            }
        }
    }
    
    open override var prefersStatusBarHidden: Bool {
        return self.statusBarHidden
    }
    
    open override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return UIStatusBarAnimation.none
    }
    
    open override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.default
    }
    //Status bar ./
    
    
    //Scroll View When Show/Hide Keyboard
    open func ResponderKeyboard() {
        if #available(iOS 11.0, *) {
            self.rootScrollView().contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
        
        self.rootScrollView().delegate = self
        self.rootScrollView().sizeToFit()
        self.rootScrollView().showsVerticalScrollIndicator = false
        self.rootScrollView().showsHorizontalScrollIndicator = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let tabHeight: CGFloat = 0.0
            let keyboardRectangle = keyboardFrame.cgRectValue
            var keyboardHeight = keyboardRectangle.height
            
            if let currentPositionY = self.frameFocus?.frame.origin.y,
                let currentPositionHeight = self.frameFocus?.frame.height {
                
                let height = self.rootScrollView().contentSize.height
                let checkPosition = height - currentPositionY - currentPositionHeight
                Log.debug("Current Position: ScrollView_Height=\(height) -- Focus_Y=\(currentPositionY) -- Focus_Height=\(currentPositionHeight) -- Check_Position=\(checkPosition) -- Height_KeyBoard=\(keyboardHeight)")
                
                if checkPosition <= keyboardHeight {
                    if keyboardHeight > tabHeight {
                        keyboardHeight = keyboardHeight - tabHeight
                    }
                    self.rootScrollView().contentSize = CGSize(width: self.rootScrollView().contentSize.width,
                                                               height: self.rootScrollView().frame.size.height+keyboardHeight)
                    var offset = self.rootScrollView().contentOffset
                    offset.y = keyboardHeight //+ 40.0
                    Log.debug("Scroll: \(self.rootScrollView().contentOffset.debugDescription) --> \(offset.debugDescription)")
                    self.rootScrollView().setContentOffset(offset, animated: true)
                }
            }
        }
    }
    
    @objc func keyboardWillHide(notification:NSNotification) {
        self.rootScrollView().contentSize = CGSize(width: self.rootScrollView().contentSize.width,
                                                   height: self.rootScrollView().frame.size.height)
        var offset = self.rootScrollView().contentOffset
        offset.y = 0
        self.rootScrollView().setContentOffset(offset, animated: true)
    }
    //Scroll View When Show/Hide Keyboard ./
    
    
    open func isDeviceX() -> Bool {
        if UIDevice().isOneOf(UIDevice.groupOfLargeX){
            return true
        }
        return false
    }
    
    open func addConstraint(newView: UIView, rootView: UIView,
                            showStatusBar: Bool = true, fullScreen: Bool = true, fullScreenX: Bool = false) {
        newView.translatesAutoresizingMaskIntoConstraints = false
        
        //let margins = view.layoutMarginsGuide
        //NSLayoutConstraint.activate([
        //    newView.leadingAnchor.constraint(equalTo: margins.leadingAnchor), //Trong Left
        //    newView.trailingAnchor.constraint(equalTo: margins.trailingAnchor) //Trong Right
        //    ])
        
        if #available(iOS 11.0, *) {
            let guide = rootView.safeAreaLayoutGuide
            newView.trailingAnchor.constraint(equalTo: guide.trailingAnchor).isActive = true //Right
            if (showStatusBar && !fullScreen) || (self.isDeviceX() && !fullScreenX) {
                newView.topAnchor.constraint(equalTo: guide.topAnchor).isActive = true //Top-guide.topAnchor(below status bar)
            }else {
                newView.topAnchor.constraint(equalTo: rootView.topAnchor).isActive = true //Top-rootView.topAnchor(above status bar)
            }
            newView.leadingAnchor.constraint(equalTo: guide.leadingAnchor).isActive = true //Left
            //newView.bottomAnchor.constraint(equalTo: guide.bottomAnchor).isActive = true //Bottom-guide.bottomAnchor
            //newView.bottomAnchor.constraint(equalTo: rootView.bottomAnchor).isActive = true //Bottom-rootView.bottomAnchor
            
            if self.isDeviceX() && fullScreenX {
                newView.trailingAnchor.constraint(equalTo: rootView.trailingAnchor).isActive = true //Right-rootView
                //newView.topAnchor.constraint(equalTo: guide.topAnchor).isActive = true //Top-guide.topAnchor(below status bar)
                //newView.topAnchor.constraint(equalTo: rootView.topAnchor).isActive = true //Top-view.topAnchor(above status bar)
                newView.leadingAnchor.constraint(equalTo: rootView.leadingAnchor).isActive = true //Left-rootView
                newView.bottomAnchor.constraint(equalTo: rootView.bottomAnchor).isActive = true //Bottom-rootView.bottomAnchor
            }else {
                newView.bottomAnchor.constraint(equalTo: guide.bottomAnchor).isActive = true //Bottom-guide.bottomAnchor
            }
            
            newView.centerXAnchor.constraint(equalTo: guide.centerXAnchor).isActive = true //Giữa X
            //newView.centerYAnchor.constraint(equalTo: guide.centerYAnchor).isActive = true //Giữa Y
            
            //newView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        } else {
            if #available(iOS 9.0, *) {
                newView.trailingAnchor.constraint(equalTo: rootView.trailingAnchor).isActive = true //Right
                if showStatusBar && !fullScreen {
                    newView.topAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor).isActive = true //Top-topLayoutGuide(below status bar)
                }else {
                    newView.topAnchor.constraint(equalTo: rootView.topAnchor).isActive = true //Top-rootView.topAnchor(above status bar)
                }
                newView.leadingAnchor.constraint(equalTo: rootView.leadingAnchor).isActive = true //Left
                newView.bottomAnchor.constraint(equalTo: rootView.bottomAnchor).isActive = true //Bottom-rootView.bottomAnchor
                newView.bottomAnchor.constraint(equalTo: bottomLayoutGuide.topAnchor).isActive = true //Bottom-bottomLayoutGuide
                
                newView.centerXAnchor.constraint(equalTo: rootView.centerXAnchor).isActive = true //Giữa X
                //newView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true //Giữa Y
            } else {
                NSLayoutConstraint(item: newView, attribute: .trailing, relatedBy: .equal,
                                   toItem: rootView, attribute: .trailing,
                                   multiplier: 1.0, constant: 0).isActive = true //Right
                if showStatusBar && !fullScreen {
                    NSLayoutConstraint(item: newView, attribute: .top, relatedBy: .equal,
                                       toItem: topLayoutGuide, attribute: .bottom,
                                       multiplier: 1.0, constant: 0).isActive = true //Top-topLayoutGuide(below status bar)
                }else {
                    NSLayoutConstraint(item: newView, attribute: .top, relatedBy: .equal,
                                       toItem: rootView, attribute: .top,
                                       multiplier: 1.0, constant: 0).isActive = true //Top-rootView.top(above status bar)
                }
                NSLayoutConstraint(item: newView, attribute: .leading, relatedBy: .equal,
                                   toItem: rootView, attribute: .leading,
                                   multiplier: 1.0, constant: 0).isActive = true //Left
                NSLayoutConstraint(item: newView, attribute: .bottom, relatedBy: .equal,
                                   toItem: rootView, attribute: .bottom,
                                   multiplier: 1.0, constant: 0).isActive = true //Bottom-rootView.bottom
                NSLayoutConstraint(item: newView, attribute: .bottom, relatedBy: .equal,
                                   toItem: bottomLayoutGuide, attribute: .top,
                                   multiplier: 1.0, constant: 0).isActive = true //Bottom-bottomLayoutGuide
                
                NSLayoutConstraint(item: newView, attribute: .centerX, relatedBy: .equal,
                                   toItem: rootView, attribute: .centerX,
                                   multiplier: 1, constant: 0).isActive = true //Giữa X
                //NSLayoutConstraint(item: newView, attribute: .centerY, relatedBy: .equal,
                //                   toItem: view, attribute: .centerY,
                //                   multiplier: 1, constant: 0).isActive = true //Giữa Y
            }
            //newView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        }
    }
    
    open func addConstraintOnScrollView(newView: UIView, scrollView: UIView) {
        if #available(iOS 9.0, *) {
            newView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor).isActive = true //Right
            newView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true //Top-scrollView.topAnchor(above status bar)
            //newView.topAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor).isActive = true //Top-topLayoutGuide(below status bar)
            newView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor).isActive = true //Left
            newView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true //Bottom-scrollView.bottomAnchor
            //newView.bottomAnchor.constraint(equalTo: bottomLayoutGuide.topAnchor).isActive = true //Bottom-bottomLayoutGuide
            newView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true //Giữa X
            newView.centerYAnchor.constraint(equalTo: scrollView.centerYAnchor).isActive = true //Giữa Y
        } else {
            NSLayoutConstraint(item: newView, attribute: .trailing, relatedBy: .equal,
                               toItem: scrollView, attribute: .trailing,
                               multiplier: 1.0, constant: 0).isActive = true //Right
            NSLayoutConstraint(item: newView, attribute: .top, relatedBy: .equal,
                               toItem: scrollView, attribute: .top,
                               multiplier: 1.0, constant: 0).isActive = true //Top-scrollView.top(above status bar)
            //NSLayoutConstraint(item: newView, attribute: .top, relatedBy: .equal,
            //                   toItem: topLayoutGuide, attribute: .bottom,
            //                   multiplier: 1.0, constant: 0).isActive = true //Top-topLayoutGuide(below status bar)
            NSLayoutConstraint(item: newView, attribute: .leading, relatedBy: .equal,
                               toItem: scrollView, attribute: .leading,
                               multiplier: 1.0, constant: 0).isActive = true //Left
            NSLayoutConstraint(item: newView, attribute: .bottom, relatedBy: .equal,
                               toItem: scrollView, attribute: .bottom,
                               multiplier: 1.0, constant: 0).isActive = true //Bottom-scrollView.bottom
            //NSLayoutConstraint(item: newView, attribute: .bottom, relatedBy: .equal,
            //                   toItem: bottomLayoutGuide, attribute: .top,
            //                   multiplier: 1.0, constant: 0).isActive = true //Bottom-bottomLayoutGuide
            NSLayoutConstraint(item: newView, attribute: .centerX, relatedBy: .equal,
                               toItem: scrollView, attribute: .centerX,
                               multiplier: 1, constant: 0).isActive = true //Giữa X
            NSLayoutConstraint(item: newView, attribute: .centerY, relatedBy: .equal,
                               toItem: scrollView, attribute: .centerY,
                               multiplier: 1, constant: 0).isActive = true //Giữa Y
        }
    }
    
    open func getFrameWithoutConstraint(showStatusBar: Bool, fullScreen: Bool = true, fullScreenX: Bool = false) -> CGRect {
        var heightStatusBar: CGFloat = UIApplication.shared.statusBarFrame.size.height
        if !showStatusBar || (showStatusBar && ((!self.isDeviceX() && fullScreen) || (self.isDeviceX() && fullScreenX))) {
            heightStatusBar = 0.0
        }
        var newFrame: CGRect = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        let device = UIDevice()
        if device.isOneOf(UIDevice.groupOfSmall) {
            newFrame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - heightStatusBar - self.tabbarHeight)
        }else if device.isOneOf(UIDevice.groupOfMedium) {
            newFrame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - heightStatusBar - self.tabbarHeight)
        }else if device.isOneOf(UIDevice.groupOfLarge) {
            newFrame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - heightStatusBar - self.tabbarHeight)
        }else if device.isOneOf(UIDevice.groupOfLargeX) {
            if #available(iOS 11.0, *) {
                if let appDelegate = UIApplication.shared.delegate {
                    if let window = appDelegate.window {
                        let safeAreaBottom = window?.safeAreaInsets.bottom ?? 0.0
                        let safeAreaLeft = window?.safeAreaInsets.left ?? 0.0
                        let safeAreaRight = window?.safeAreaInsets.right ?? 0.0
                        let safeAreaTop = window?.safeAreaInsets.top ?? 0.0
                        Log.debug("SafeArea --> Bottom:\(safeAreaBottom) - Left:\(safeAreaLeft) - Right:\(safeAreaRight) - Top:\(safeAreaTop)")
                        
                        newFrame = CGRect(x: 0, y: 0,
                                          width: UIScreen.main.bounds.width,
                                          height: UIScreen.main.bounds.height - safeAreaBottom - heightStatusBar - self.tabbarHeight)
                    }
                }
            }
        }
        return newFrame
    }
    
    open func getFrameContent(_ frmEdit: CGRect) -> CGRect {
        return CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: frmEdit.height)
    }
    
    
    open func viewDidLoad(_ isRemoveView: Bool = true,_ isShowStatusBar: Bool = true,
                          _ isFullScreen: Bool = true,_ isFullScreenX: Bool = false) {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = UIColor.clear
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.backItem?.title = ""
        //NSLocalizedString("navigation.button.back.title", comment: "")
        
        //Status bar
        let device = UIDevice()
        if isShowStatusBar {
            if device.isOneOf(UIDevice.groupOfLargeX) {
                self.statusBarHidden = false
            }else {
                if #available(iOS 9.0, *) {
                    self.statusBarHidden = false
                } else {
                    UIApplication.shared.setStatusBarHidden(false, with: UIStatusBarAnimation.none)
                }
            }
        }else {
            if device.isOneOf(UIDevice.groupOfLargeX) {
                self.statusBarHidden = true
            }else {
                if #available(iOS 9.0, *) {
                    self.statusBarHidden = true
                } else {
                    UIApplication.shared.setStatusBarHidden(true, with: UIStatusBarAnimation.none)
                }
            }
        }
        
        //Remove all view in rootView
        if isRemoveView {
            self.clearAllSubView(self.rootView())
        }
        
        //Add constraint Safe Area
        self.addConstraint(newView: self.rootScrollView(), rootView: self.view,
                           showStatusBar: isShowStatusBar, fullScreen: isFullScreen, fullScreenX: isFullScreenX)
        self.addConstraintOnScrollView(newView: self.rootView(), scrollView: self.rootScrollView())
        
        //Get frame default with out Constraint
        self.frameIphone = self.getFrameWithoutConstraint(showStatusBar: isShowStatusBar, fullScreen: isFullScreen, fullScreenX: isFullScreenX)
        self.rootScrollView().contentSize = CGSize(width: self.frameIphone!.width, height: self.frameIphone!.height)
        
        self.ResponderKeyboard()
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    open override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    open override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    open func showDialogOK(_ title: String,_ message: String, completionOK: (@escaping () -> (Void))) {
        let attributedTitle = NSAttributedString(string: title, attributes: [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 20),
            NSAttributedString.Key.foregroundColor : UIColor.black
            ])
        let attributedMessage = NSAttributedString(string: message, attributes: [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15),
            NSAttributedString.Key.foregroundColor : UIColor.black
            ])
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let backView = alert.view.subviews.last?.subviews.last
        backView?.layer.cornerRadius = 10.0
        backView?.backgroundColor = UIColor.white
        alert.setValue(attributedTitle, forKey: "attributedTitle") //Title
        alert.setValue(attributedMessage, forKey: "attributedMessage") //Message
        alert.view.tintColor = UIColor.black //Button
        
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
            alert.dismiss(animated: true, completion: nil)
            completionOK()
        }
        alert.addAction(OKAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    //Progress View
    public var loadingView = LoadingView(frame: CGRect(origin: .zero,
                                                       size: CGSize(width: UIScreen.main.bounds.width,
                                                                    height: UIScreen.main.bounds.height)))
    public var loadingProgressView = LoadingProgressView(frame: UIScreen.main.bounds)
    
    
    open func showProgress() {
        self.view.addSubview(self.loadingView)
    }
    
    open func hideProgress() {
        self.loadingView.removeFromSuperview()
    }
    
    open func showProgressDialog(_ valueText: String) {
        self.loadingProgressView.tag = 987654
        self.loadingProgressView.updateStatus(value: valueText)
        self.view.addSubview(self.loadingProgressView)
    }
    
    open func updateProgressDialog(_ valueText: String) {
        if let loadingProgressView = self.view.viewWithTag(987654) {
            (loadingProgressView as? LoadingProgressView)?.updateStatus(value: valueText)
        }
    }
    
    open func hideProgressDialog() {
        self.loadingProgressView.removeFromSuperview()
    }
    //Progress View ./
    
    
    open func autoScrollCellTableView(_ view: UIView,_ cell: UITableViewCell,_ addMore: CGFloat = 0.0) {
        let viewFocus = UIView(frame: CGRect(
            x: view.frame.origin.x,
            y: view.frame.origin.y + (cell.parentTableView?.frame.origin.y ?? 0.0) + addMore,
            width: view.frame.width,
            height: view.frame.height))
        self.frameFocus = viewFocus
    }
    
    open func autoScrollCellCollectionView(_ view: UIView,_ cell: UICollectionViewCell,_ addMore: CGFloat = 0.0) {
        let viewFocus = UIView(frame: CGRect(
            x: view.frame.origin.x,
            y: view.frame.origin.y + (cell.parentCollectionView?.frame.origin.y ?? 0.0) + addMore,
            width: view.frame.width,
            height: view.frame.height))
        self.frameFocus = viewFocus
    }
    
    //Language case 2
    func updateLanguageOnView(_ locale: String) { //LangEN-LangVI
        Preferences.shared.setCurrentLocale(locale)
        self.rootView().onUpdateLanguage()
    }
    //Language case 2./
}




private extension UIView {
    func parentView<T: UIView>(of type: T.Type) -> T? {
        guard let view = self.superview else {
            return nil
        }
        return (view as? T) ?? view.parentView(of: T.self)
    }
}

private extension UITableViewCell {
    //Recursively
    var myTableView: UITableView? {
        return self.parentView(of: UITableView.self)
    }
    
    //Using loop
    var parentTableView: UITableView? {
        var view = self.superview
        while (view != nil && view!.isKind(of: UITableView.self) == false) {
            view = view!.superview
        }
        return view as? UITableView
    }
}

private extension UICollectionViewCell {
    //Recursively
    var myCollectionView: UICollectionView? {
        return self.parentView(of: UICollectionView.self)
    }
    
    //Using loop
    var parentCollectionView: UICollectionView? {
        var view = self.superview
        while (view != nil && view!.isKind(of: UICollectionView.self) == false) {
            view = view!.superview
        }
        return view as? UICollectionView
    }
}
