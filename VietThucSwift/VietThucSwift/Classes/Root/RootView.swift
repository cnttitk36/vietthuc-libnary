//
//  RootView.swift
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit
import DeviceKit

open class RootView: UIView {
    public var rootVC: RootViewController?
    
    open func getRootViewController() -> RootViewController? {
        return self.rootVC
    }
    
    open func xibSetup(frame: CGRect, rootVC: RootViewController?) {
        self.rootVC = rootVC
        if let view = UINib(nibName: className(), bundle: nil).instantiate(withOwner: self, options: nil).first as? UIView {
            view.frame = frame
            view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.addSubview(view)
        }
        self.setControlView()
        self.applyFont()
        self.adjustView()
        self.setActionView()
        self.initObserverLanguage()
    }
    
    open func setControlView() {}
    open func setActionView() {}
    open func adjustView() {}
    open func applyFont() {}
    open func updateUI() {}
    
    open func className() -> String {
        return String(describing: type(of: self)) // NOTES: --> xib Name from self
    }
    
    open func dismissKeyboard() {
        self.endEditing(true)
    }
    
    open func showMessage(_ messageTitle: String, _ messageContent: String) {
        self.getRootViewController()?.showDialogOK(messageTitle, messageContent, completionOK: {})
    }
    
    
    //---------- Language case 1
    open var listLanguage_View = [UIView]()
    
    open func initObserverLanguage() {
        self.setupLanguage()
        // Register ObserverLanguage
        NotificationCenter.default.addObserver(self, selector: #selector(self.observerDidChangeLanguage), name: NSNotification.Name(rawValue: "MobiAppsLanguages"), object: nil)
    }
    
    @objc func observerDidChangeLanguage(_ notification: Notification) {
        Log.debug("Receiver ObserverLanguage:\(Language.getLanguageName(Language.getCurrentLanguageCode()))\n")
        self.setupLanguage()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "MobiAppsLanguages"), object: nil);
    }
    
    open func setupLanguage() {
        self.listLanguage_View.removeAll()
        self.getListViewLanguage(self)
        
        for i in 0..<listLanguage_View.count {
            if let label = listLanguage_View[i] as? UILabel {
                if Language.getTextWithKey(label.getKey_Lang()) != "" {
                    label.text = Language.getTextWithKey(label.getKey_Lang())
                }
            } else if let textField = listLanguage_View[i] as? UITextField {
                if Language.getTextWithKey(textField.getKey_Lang()) != "" {
                    textField.placeholder = Language.getTextWithKey(textField.getKey_Lang())
                }
            } else if let button = listLanguage_View[i] as? UIButton {
                if Language.getTextWithKey(button.getKey_Lang()) != "" {
                    button.setTitle(Language.getTextWithKey(button.getKey_Lang()), for: .normal)
                }
            }
        }
    }
    
    open func getListViewLanguage(_ parentView: UIView) {
        for view in parentView.subviews {
            if let label = view as? UILabel {
                self.listLanguage_View.append(label)
            } else if let textField = view as? UITextField {
                self.listLanguage_View.append(textField)
            } else if let button = view as? UIButton {
                self.listLanguage_View.append(button)
            } else {
                self.getListViewLanguage(view)
            }
        }
    }
    //---------- Language case 1 ./
    
    
    //Progress on View
    open var loadingOnView = LoadingView(frame: CGRect(origin: .zero,
                                                       size: CGSize.init(width: UIScreen.main.bounds.width,
                                                                         height: UIScreen.main.bounds.height)))
    open func showProgressOnView() {
        self.addSubview(self.loadingOnView)
    }
    
    open func hideProgressOnView() {
        self.loadingOnView.removeFromSuperview()
    }
    //Progress on View ./
}
