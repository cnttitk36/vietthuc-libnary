//
//  RootInputView.swift
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

open class RootInputView: RootView, UITextFieldDelegate, UITextViewDelegate {
    
    //UITextFieldDelegate, UITextViewDelegate
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.endEditing(true)
    }

    open func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    open func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        textView.resignFirstResponder()
        return true
    }

    //Show keyboard move layout with position RootViewController --> frameFocus
    open func textFieldDidBeginEditing(_ textField: UITextField) {
        self.getRootViewController()?.frameFocus = textField //get frame when focous textField
    }

    open func textFieldDidEndEditing(_ textField: UITextField) {
    }

    open func textViewDidBeginEditing(_ textView: UITextView) {
        self.getRootViewController()?.frameFocus = textView //get frame when focous textView
    }

    public func textViewDidEndEditing(_ textView: UITextView) {
    }
    //UITextFieldDelegate, UITextViewDelegate ./
}
