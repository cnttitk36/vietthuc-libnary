//
//  StackView.swift
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

public class StackView: UIView {
    private var top: Node? = Node()
    
    public func push(key: UIView) -> UIView? {
        // Nếu phần tử top Stack rỗng --> Tạo phần tử đầu tiên trong Stack
        if top == nil {
            top = Node()
        }
        
        // Nếu key phần tử top Stack rỗng
        if top?.key ==  nil {
            top?.key = key
            return peek()
        } else { // Ngược lại nếu key phần tử top Stack không rỗng --> Thì tạo một nút mới và gắn key vào nút --> Liên kết với phần tử top Stack --> Gán nút mới tạo là top Stack
            let newNode = Node()
            newNode.key = key
            newNode.next = top
            top = newNode
            return peek()
        }
    }
    
    public func pop() -> UIView? {
        // Nếu phần tử top Stack rỗng --> dừng
        guard top != nil else {
            return nil
        }
        
        // Nếu key phần tử top Stack rỗng --> dừng
        guard top?.key != nil else {
            return nil
        }
        
        // Nếu phần tử sau top Stack không rỗng --> Gán phần tử sau đó = top Stack
        if let nextNode = top?.next {
            top = nextNode
        } else {
            top = nil
        }
        
        return peek()
    }
    
    public func peek() -> UIView? {
        // Kiểm tra xem danh sách có rỗng ko
        guard let topItem = top?.key else {
            // Nếu rỗng thì trả về nil
            return nil
        }
        
        // Nếu không rỗng thì trả về phần tử top Stack
        return topItem
    }
    
    public func popEquals(view: UIView) -> UIView? {
        // Nếu phần tử top Stack rỗng --> dừng
        guard top != nil else {
            return nil
        }
        
        //Duyệt các phần tử trong Stack
        while top?.key != nil {
            if top?.key.classForCoder.description() == view.classForCoder.description() { // Nếu tồn tại --> trả về
                return top?.key
            }else{
                top = top?.next!
            }
        }
        return nil
    }
    
    public func pushEquals(view: UIView) -> UIView? {
        // Nếu phần tử top Stack rỗng --> Tạo phần tử đầu tiên trong Stack
        if top == nil {
            top = Node()
        }
        
        // Nếu key phần tử top Stack rỗng
        if top?.key ==  nil {
            top?.key = view
            return peek()
        } else { // Ngược lại nếu key phần tử top Stack không rỗng
            //Duyệt các phần tử trong Stack
            while top != nil && top?.key != nil {
                if top?.key.classForCoder.description() == view.classForCoder.description() { // Nếu tồn tại --> trả về
                    return peek()
                }else{
                    top = top?.next
                }
            }
            // Ngược lại nếu không tồn tại phần tử trong Stack --> Tạo mới và thêm vào Stack
            let newNode = Node()
            newNode.key = view
            newNode.next = top
            top = newNode
            return peek()
        }
    }
    
    public func pushUIView(view: UIView){
        for item in self.subviews {
            item.removeFromSuperview()
        }
        
        if let temp = self.push(key: view) {
            self.addSubview(temp)
        }
    }
    
    public func popUIView() {
        if let temp = self.pop() {
            for item in self.subviews {
                item.removeFromSuperview()
            }
            self.addSubview(temp)
        }
    }
}
