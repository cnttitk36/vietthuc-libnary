//
//  Author.swift
//  Pods-VietThucSwift_Example
//
//  Created by Tran Viet Thuc on 12/31/18.
//

import UIKit

public class Author: NSObject {
    
    public override init() {
        super.init()
    }
    
    public func getName() -> String {
        return "Trần Việt Thức"
    }
    
    public func getPhone() -> String {
        return "038.99.55.141"
    }
    
    public func getAddress() -> String {
        return "Quy Nhơn - Bình Định"
    }
}
