//
//  Language.swift
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

extension UIView {
    @objc func onUpdateLanguage() {
        for subView: UIView in self.subviews {
            subView.onUpdateLanguage()
        }
    }
}

extension UILabel {
    private struct AssociatedKeys {
        static var key_Lang: String = ""
    }
    
    public var Key_Lang: String? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.key_Lang) as? String
        }
        
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(
                    self,
                    &AssociatedKeys.key_Lang,
                    newValue as NSString?,
                    .OBJC_ASSOCIATION_RETAIN_NONATOMIC
                )
            }
        }
    }
    
    public func setKey_Lang(_ key: String) {
        self.Key_Lang = key
        if let value = LocalizationHelper.shared.localized(key), value != "" {
            self.text = value
        }
    }
    
    public func getKey_Lang() -> String {
        return self.Key_Lang ?? ""
    }
    
    //Case update language 2
    @objc override func onUpdateLanguage() {
        super.onUpdateLanguage()
        if let value = LocalizationHelper.shared.localized(self.Key_Lang), value != "" {
            self.text = value
        }
    }
}

extension UITextField {
    private struct AssociatedKeys {
        static var key_Lang: String = ""
    }
    
    public var Key_Lang: String? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.key_Lang) as? String
        }
        
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(
                    self,
                    &AssociatedKeys.key_Lang,
                    newValue as NSString?,
                    .OBJC_ASSOCIATION_RETAIN_NONATOMIC
                )
            }
        }
    }
    
    public func setKey_Lang(_ key: String) {
        self.Key_Lang = key
        if let value = LocalizationHelper.shared.localized(key), value != "" {
            self.placeholder = value
        }
    }
    
    public func getKey_Lang() -> String {
        return self.Key_Lang ?? ""
    }
    
    //Case update language 2
    @objc override func onUpdateLanguage() {
        super.onUpdateLanguage()
        if let value = LocalizationHelper.shared.localized(self.Key_Lang), value != "" {
            self.placeholder = value
        }
    }
}

extension UIButton {
    private struct AssociatedKeys {
        static var key_Lang: String = ""
    }
    
    public var Key_Lang: String? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.key_Lang) as? String
        }
        
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(
                    self,
                    &AssociatedKeys.key_Lang,
                    newValue as NSString?,
                    .OBJC_ASSOCIATION_RETAIN_NONATOMIC
                )
            }
        }
    }
    
    public func setKey_Lang(_ key: String) {
        self.Key_Lang = key
        if let value = LocalizationHelper.shared.localized(key), value != "" {
             self.setTitle(value, for: state)
        }
    }
    
    public func getKey_Lang() -> String {
        return self.Key_Lang ?? ""
    }
    
    //Case update language 2
    @objc override func onUpdateLanguage() {
        super.onUpdateLanguage()
        if let value = LocalizationHelper.shared.localized(self.Key_Lang), value != "" {
            self.setTitle(value, for: state)
        }
    }
}


//Language.initial()
public class Language: NSObject {
    public static var bundle: Bundle? = nil
    
    public class func initial() {
        let current = self.getCurrentLanguageCode()
        self.setLanguage(current)
    }
    
    public class func setLanguage(_ languageCode: String) {
        UserDefaults.standard.set(languageCode, forKey: "curren_language")
        UserDefaults.standard.synchronize()
        var filePath = "\(self.appDocumentDirectory())/\(languageCode).lproj"
        let exists = FileManager.default.fileExists(atPath: filePath)
        if !exists {
            filePath = Bundle.main.path(forResource: languageCode, ofType: "lproj")!
        }
        Language.bundle = Bundle(path: filePath)
    }
    
    public class func getCurrentLanguageCode() -> String { //LangEN-LangVI
        let defs = UserDefaults.standard
        let lang = defs.object(forKey: "curren_language") as! String?
        if let size = lang?.count, size != 0 {
            return lang!
        }else {
            return "vi"
        }
    }
    
    public class func getTextWithKey(_ key: String) -> String {
        if let bundle = Language.bundle, key != "" {
            var result = bundle.localizedString(forKey: key, value: nil, table: nil)
            if result == "" {
                result = key
            }
            return result
        }
        return key
    }
    
    public class func appDocumentDirectory() -> String {
        return NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    }
    
    public class func getLanguageCode(_ language: String) -> String {
        switch language {
        case "English":
            return "en"
        case "Việt Nam":
            return "vi"
        default:
            return "en"
        }
    }
    
    public class func getLanguageName(_ languageCode: String) -> String {
        switch languageCode {
        case "en":
            return "English"
        case "vi":
            return "Việt Nam"
        default:
            return "English"
        }
    }
}
