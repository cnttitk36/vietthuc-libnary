//
//  Preferences.swift
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import Foundation

public class Preferences {
    public static let kCurrentLocale = "CurrentLocale"
    public static let kDefaultLocale = LangVI
    
    public static let shared = Preferences()
    
    public func currentLocale() -> String {
        if let locale = UserDefaults.standard.value(forKey: Preferences.kCurrentLocale) {
            return locale as! String
        }
        return Preferences.kDefaultLocale
    }
    
    public func setCurrentLocale(_ locale: String) {
        UserDefaults.standard.set(locale, forKey: Preferences.kCurrentLocale)
        UserDefaults.standard.synchronize()
    }
}
