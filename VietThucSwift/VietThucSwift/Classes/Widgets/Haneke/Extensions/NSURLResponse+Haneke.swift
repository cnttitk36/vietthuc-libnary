//
//  NSURLResponse+Haneke.swift
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import Foundation

extension URLResponse {
    
    func hnk_validateLength(ofData data: Data) -> Bool {
        let expectedContentLength = self.expectedContentLength
        if (expectedContentLength > -1) {
            let dataLength = data.count
            return Int64(dataLength) >= expectedContentLength
        }
        return true
    }
    
}
