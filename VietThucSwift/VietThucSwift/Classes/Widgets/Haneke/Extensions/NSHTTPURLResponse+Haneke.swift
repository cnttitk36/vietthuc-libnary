//
//  NSHTTPURLResponse+Haneke.swift
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import Foundation

extension HTTPURLResponse {

    func hnk_isValidStatusCode() -> Bool {
        switch self.statusCode {
        case 200...201:
            return true
        default:
            return false
        }
    }

}
