//
//  WidgetLocationManager.swift
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import Foundation
import CoreLocation

public class LocationManagerWidget: NSObject, CLLocationManagerDelegate {
    public static let share = LocationManagerWidget()
    
    private let manager = CLLocationManager()
    private let geocoder = CLGeocoder()
    
    public typealias Action = (CLLocation?) -> (Void)
    public var completion: Action?
    public var currentLocation: CLLocation?
    public var locationName: String? //422 Đường Nguyễn Thị Minh Khai
    public var locality: String? //Tp. Qui Nhơn
    public var street: String? //Đường Nguyễn Thị Minh Khai
    public var country: String? //Việt Nam
    public var administrativeArea: String? //Tỉnh Bình Định
    
    public func initLocation() {
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse {
            manager.requestWhenInUseAuthorization()
        } else {
            manager.startUpdatingLocation()
        }
        Log.debug("Request Location")
    }
    
    public func requestLocation(_ completion: (@escaping (CLLocation?) -> (Void)), update: Bool = false) {
        self.completion = completion
        if self.currentLocation == nil || update == true {
            self.initLocation()
        }else {
            if let location = self.currentLocation {
                self.completion?(location)
                self.completion = nil
            }else {
                self.initLocation()
            }
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        Log.debug("Response Location: \(location.description)")
        
        if location.horizontalAccuracy < 200 {
            self.currentLocation = location
            manager.stopUpdatingLocation()
            
            geocoder.reverseGeocodeLocation(location, completionHandler: {(placemarks, error) in
                if (error != nil) {
                    Log.debug("Error in reverseGeocode")
                }
                
                let placemark = placemarks! as [CLPlacemark] // Place details
                if placemark.count > 0 {
                    let placemark = placemarks![0]
                    let locationName = placemark.name ?? "" //Location name
                    let locality = placemark.locality ?? "" //Locality
                    let street = placemark.thoroughfare ?? "" // Street address
                    let country = placemark.country ?? ""// Country
                    let administrativeArea = placemark.administrativeArea ?? "" //AdministrativeArea
                    
                    Log.debug(locationName)//422 Đường Nguyễn Thị Minh Khai
                    Log.debug(locality)//Tp. Qui Nhơn
                    Log.debug(street)//Đường Nguyễn Thị Minh Khai
                    Log.debug(country)//Việt Nam
                    Log.debug(administrativeArea)//Tỉnh Bình Định
                    self.locationName = locationName
                    self.locality = locality
                    self.street = street
                    self.country = country
                    self.administrativeArea = administrativeArea
                    
                    let userLocationString = "\(street), \(locality), \(administrativeArea), \(country)"
                    Log.debug("Address --> "+userLocationString)
                }
            })
            self.completion?(self.currentLocation)
            self.completion = nil
        }
    }
}
