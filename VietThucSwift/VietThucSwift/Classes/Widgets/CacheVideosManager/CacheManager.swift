//
//  CacheManager.swift
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

public enum Result<T> {
    case success(T)
    case failure(NSError)
}

class ExCacheVideosManager {
    func load() {
        CacheVideosManager.shared.getFile(stringUrl: "http://techslides.com/demos/sample-videos/small.mp4", completionHandler: { result in
            switch result {
            case .success(let url):
                Log.debug("\(url.absoluteString)")
            case .failure(let error):
                Log.debug("\(error.domain)")
            }
        })
    }
}

public class CacheVideosManager {
    public static let shared = CacheVideosManager()
    private let fileManager = FileManager.default
    private lazy var mainDirectoryUrl: URL = {
        let documentsUrl = self.fileManager.urls(for: .cachesDirectory, in: .userDomainMask).first!
        return documentsUrl
    }()
    
    public func getFile(stringUrl: String, completionHandler: @escaping (Result<URL>) -> Void ) {
        let file = self.directoryFor(stringUrl: stringUrl)
        
        //return file path if already exists in cache directory
        guard !fileManager.fileExists(atPath: file.path)  else {
            Log.debug("Cache videos exists...")
            completionHandler(Result.success(file))
            return
        }
        
        DispatchQueue.global().async {
            let linkURL = URL(string: stringUrl)!
            if let videoData = NSData(contentsOf: linkURL) {
                Log.debug("Begin write cache videos...")
                
                videoData.write(to: file, atomically: true)
                DispatchQueue.main.async {
                    completionHandler(Result.success(file))
                }
            } else {
                DispatchQueue.main.async {
                    completionHandler(Result.failure(NSError(domain: "Cache can't download video", code: 400, userInfo: nil)))
                }
            }
        }
    }
    
    public func getFileWithHeader(stringUrl: String, header: [String: String], completionHandler: @escaping (Result<URL>) -> Void ) {
        let file = self.directoryFor(stringUrl: stringUrl)
        
        //return file path if already exists in cache directory
        guard !fileManager.fileExists(atPath: file.path)  else {
            Log.debug("Cache videos exists...")
            completionHandler(Result.success(file))
            return
        }
        
        DispatchQueue.global().async {
            let session = URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: URL(string: stringUrl)!)
            request.httpMethod = "GET"
            for (key, value) in header{
                request.addValue(value, forHTTPHeaderField: key)
            }
            
            let task = session.dataTask(with: request, completionHandler: {
                data, response, error in
                if error == nil {
                    if let response = response as? HTTPURLResponse {
                        if response.statusCode == 200 {
                            if let data = data {
                                if let _ = try? data.write(to: file, options: Data.WritingOptions.atomic) {
                                    DispatchQueue.main.async {
                                        completionHandler(Result.success(file))
                                    }
                                }else {
                                    DispatchQueue.main.async {
                                        completionHandler(Result.failure(NSError(domain: "Cache can't save video", code: 400, userInfo: nil)))
                                    }
                                }
                            }else {
                                DispatchQueue.main.async {
                                    completionHandler(Result.failure(NSError(domain: "Cache download video nil", code: 400, userInfo: nil)))
                                }
                            }
                        }
                    }
                }else {
                    DispatchQueue.main.async {
                        completionHandler(Result.failure(NSError(domain: "Cache can't download video", code: 400, userInfo: nil)))
                    }
                }
            })
            task.resume()
        }
    }
    
    private func directoryFor(stringUrl: String) -> URL {
        let fileURL = URL(string: stringUrl)!.lastPathComponent
        let file = self.mainDirectoryUrl.appendingPathComponent(fileURL)
        return file
    }
    
    public func removeAll() {
        let documentPath = mainDirectoryUrl.path
        do {
            let fileNames = try fileManager.contentsOfDirectory(atPath: "\(documentPath)")
            Log.debug("all files in cache: \(fileNames)")
            for fileName in fileNames {
                
                if (fileName.hasSuffix(".png"))
                {
                    let filePathName = "\(documentPath)/\(fileName)"
                    try fileManager.removeItem(atPath: filePathName)
                }
            }
            
            let files = try fileManager.contentsOfDirectory(atPath: "\(documentPath)")
            Log.debug("all files in cache after deleting images: \(files)")
        } catch {
            Log.debug("Could not clear temp folder: \(error)")
        }
    }
}

