//
//  LogManager.swift
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import Foundation

public struct Log {
    public static func debug(_ text: String?) {
        #if DEBUG
        guard let text = text else { return }
        print(text)
        #else
        #endif
    }
    
    public static func release(_ text: String?) {
        #if DEBUG
        #else
        guard let text = text else { return }
        print(text)
        #endif
    }
}
