//
//  MultiSliderExampleView.swift
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

class MultiSliderExampleView: UIView {

    @IBOutlet var _slider: MultiSlider!
    
    func initMultiSlider() {
        _slider.addTarget(self, action: #selector(_sliderChangedPriceRange(_:)), for: .valueChanged)
        _slider.addTarget(self, action: #selector(_sliderDragEndedPriceRange(_:)), for: . touchUpInside)
        
        let thumb = UIImage(named: "slider_point.png")?.resizeImage(targetSize: CGSize(width: 32, height: 32))
        _slider.thumbImage = thumb
        //_sliderPriceRange.minimumImage = thumb
        //_sliderPriceRange.maximumImage = thumb
        
        _slider.orientation = .horizontal // default is .vertical
        _slider.minimumValue = 0 // Giá trị khoảng cách nhỏ nhất giữa 2 track
        _slider.maximumValue = 100 // Giá trị khoảng cách lớn nhất giữa 2 track
        _slider.thumbCount = 2 // Số lượng thumb track
        
        _slider.disabledThumbIndices = [5]
        
        // .notAnAttribute = không hiển thị title description value
        _slider.valueLabelPosition = .top
        
        // Default is 0.0 - kích thước nhỏ nhất được chấp nhận khi thay đổi giá trị
        _slider.snapStepSize = 5
        
        // Track cuối hiển thị giá trị khoảng cách giữa 2 track thay vì giá trị  tuyệt đối
        _slider.isValueLabelRelative = false
        
        // Tiếp đầu ngữ title description value
        _slider.valueLabelFormatter.positiveSuffix = " triệu"
        
        _slider.tintColor = UIColor.rgb(fromHexString: "#7477fa") // Màu của track vùng được chọn
        _slider.outerTrackColor = .black // Màu của track ngoài vùng được chọn
        _slider.trackWidth = 2 // Độ dày của đường track đường
        
        _slider.hasRoundTrackEnds = true // Làm tròn track cuối khi track đầu thay đổi
        _slider.showsThumbImageShadow = false // Hiển thị shadow
    }
    
    @objc func _sliderChangedPriceRange(_ slider: MultiSlider) {
        print("Changed-\(slider.value)")
    }
    
    @objc func _sliderDragEndedPriceRange(_ slider: MultiSlider) {
        print("DragEnded-\(slider.value)")
    }
    
    func setValueSlider(_ slider: MultiSlider) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            slider.value = [0.4, 2.8]
            slider.valueLabelPosition = .top
        }
        
        //DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
        //    self._sliderPriceRange.thumbCount = 5
        //    self._sliderPriceRange.valueLabelPosition = .right
        //    self._sliderPriceRange.isValueLabelRelative = true
        //}
    }
}

private extension UIImage {
    func resizeImage(targetSize: CGSize) -> UIImage {
        let size = self.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}

private extension UIColor {
    class func rgb(fromHexString: String) -> UIColor {
        var cString = fromHexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString = (cString as NSString).substring(from: 1)
        }
        
        if (cString.count != 6) {
            return UIColor.gray
        }
        
        let rString = (cString as NSString).substring(to: 2)
        let gString = ((cString as NSString).substring(from: 2) as NSString).substring(to: 2)
        let bString = ((cString as NSString).substring(from: 4) as NSString).substring(to: 2)
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        Scanner(string: rString).scanHexInt32(&r)
        Scanner(string: gString).scanHexInt32(&g)
        Scanner(string: bString).scanHexInt32(&b)
        
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
    }
}
