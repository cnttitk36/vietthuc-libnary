//
//  ESPullToRefresh+Manager.swift
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import Foundation

open class ESRefreshDataManager {
    
    static let sharedManager = ESRefreshDataManager.init()
    
    static let lastRefreshKey: String = "com.espulltorefresh.lastRefreshKey"
    static let expiredTimeIntervalKey: String = "com.espulltorefresh.expiredTimeIntervalKey"
    open var lastRefreshInfo = [String: Date]()
    open var expiredTimeIntervalInfo = [String: TimeInterval]()
    
    public required init() {
        if let lastRefreshInfo = UserDefaults.standard.dictionary(forKey: ESRefreshDataManager.lastRefreshKey) as? [String: Date] {
            self.lastRefreshInfo = lastRefreshInfo
        }
        if let expiredTimeIntervalInfo = UserDefaults.standard.dictionary(forKey: ESRefreshDataManager.expiredTimeIntervalKey) as? [String: TimeInterval] {
            self.expiredTimeIntervalInfo = expiredTimeIntervalInfo
        }
    }
    
    open func date(forKey key: String) -> Date? {
        let date = lastRefreshInfo[key]
        return date
    }
    
    open func setDate(_ date: Date?, forKey key: String) {
        lastRefreshInfo[key] = date
        UserDefaults.standard.set(lastRefreshInfo, forKey: ESRefreshDataManager.lastRefreshKey)
        UserDefaults.standard.synchronize()
    }
    
    open func expiredTimeInterval(forKey key: String) -> TimeInterval? {
        let interval = expiredTimeIntervalInfo[key]
        return interval
    }
    
    open func setExpiredTimeInterval(_ interval: TimeInterval?, forKey key: String) {
        expiredTimeIntervalInfo[key] = interval
        UserDefaults.standard.set(expiredTimeIntervalInfo, forKey: ESRefreshDataManager.expiredTimeIntervalKey)
        UserDefaults.standard.synchronize()
    }
    
    open func isExpired(forKey key: String) -> Bool {
        guard let date = date(forKey: key) else {
            return true
        }
        guard let interval = expiredTimeInterval(forKey: key) else {
            return false
        }
        if date.timeIntervalSinceNow < -interval {
            return true // Expired
        }
        return false
    }
    
    open func isExpired(forKey key: String, block: ((Bool) -> ())?) {
        DispatchQueue.global().async {
            [weak self] in
            let result = self?.isExpired(forKey: key) ?? true
            DispatchQueue.main.async(execute: {
                block?(result)
            })
        }
    }
    
    public static func clearAll() {
        self.clearLastRefreshInfo()
        self.clearExpiredTimeIntervalInfo()
    }
    
    public static func clearLastRefreshInfo() {
        UserDefaults.standard.set(nil, forKey: ESRefreshDataManager.lastRefreshKey)
    }
    
    public static func clearExpiredTimeIntervalInfo() {
        UserDefaults.standard.set(nil, forKey: ESRefreshDataManager.expiredTimeIntervalKey)
    }
    
}
