//
//  BaseCollectionViewLayout.swift
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

public protocol BaseCollectionViewLayoutDelegate: class {
    func customViewLayout(colletionViewLayout: BaseCollectionViewLayout, insetForSection section: Int) -> UIEdgeInsets
    
    func customViewLayout(collectionViewLayout: BaseCollectionViewLayout, sizeForHeaderSection section: Int) -> CGSize
    
    func customViewLayout(collecitionViewLayout: BaseCollectionViewLayout, numberColsInSection section: Int) -> Int
    
    func customViewLayout(collecitionViewLayout: BaseCollectionViewLayout, sizeForItem indexPath: IndexPath) -> CGSize
    
    func customViewLayout(collectionViewLayout: BaseCollectionViewLayout, sizeForFooterSection section: Int) -> CGSize
}

public extension BaseCollectionViewLayoutDelegate {
    func customViewLayout(colletionViewLayout: BaseCollectionViewLayout, insetForSection section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func customViewLayout(collectionViewLayout: BaseCollectionViewLayout, sizeForHeaderSection section: Int) -> CGSize {
        return CGSize.zero
    }
    
    func customViewLayout(collecitionViewLayout: BaseCollectionViewLayout, numberColsInSection section: Int) -> Int {
        return 1
    }
    
    func customViewLayout(collecitionViewLayout: BaseCollectionViewLayout, sizeForItem indexPath: IndexPath) -> CGSize {
        return CGSize.zero
    }
    
    func customViewLayout(collectionViewLayout: BaseCollectionViewLayout, sizeForFooterSection section: Int) -> CGSize {
        return CGSize.zero
    }
}

public class BaseCollectionViewLayout : UICollectionViewFlowLayout, BaseCollectionViewLayoutDelegate {
    public var widthCell : Double = 100.0
    public var heightCell : Double = 50.0
    public var spaceCell : Double = 2.0
    public var maxColumns: Int = 1
    public var widthContent: CGFloat = 0
    public var heightContent: CGFloat = 0
    
    private var listCellAttributesCache: Array<UICollectionViewLayoutAttributes> = []
    private var listHeaderAttributesCache: Array<UICollectionViewLayoutAttributes> = []
    private var listFooterAttributesCache: Array<UICollectionViewLayoutAttributes> = []
    
    public weak var delegate: BaseCollectionViewLayoutDelegate?
    
    private func turn_Right() -> Bool {
        if UIApplication.shared.statusBarOrientation == .landscapeLeft {//Turn right device
            return true
        }else if UIApplication.shared.statusBarOrientation == .landscapeRight {//Turn left device
            return false
        }
        return false
    }
    
    public override func prepare() {
        super.prepare()
        guard listCellAttributesCache.isEmpty,
              listHeaderAttributesCache.isEmpty,
              //listFooterAttributesCache.isEmpty,
              let collectionView = collectionView else {
            return
        }
        
        let fixedDimension: CGFloat
        if scrollDirection == .vertical {
            fixedDimension = collectionView.frame.width - (collectionView.contentInset.left + collectionView.contentInset.right)
            self.widthContent = fixedDimension
        } else {
            //.horizontal
            if self.turn_Right() {
                fixedDimension = collectionView.frame.height - (collectionView.contentInset.top + collectionView.contentInset.bottom)
                self.widthContent = fixedDimension
            }else {
                fixedDimension = collectionView.frame.height - (collectionView.contentInset.top + collectionView.contentInset.bottom)
                self.widthContent = fixedDimension
            }
        }
        
        let cellSize = (delegate ?? self).customViewLayout(collecitionViewLayout: self, sizeForItem: IndexPath.init(row: 0, section: 0))
        self.widthCell = Double(cellSize.width)
        self.heightCell = Double(cellSize.height)
        
        self.maxColumns = (delegate ?? self).customViewLayout(collecitionViewLayout: self, numberColsInSection: 0)
        
        var additionalSectionSpacing: CGFloat = 0
        
        for section in 0..<collectionView.numberOfSections {
            let sectionInset = (delegate ?? self).customViewLayout(colletionViewLayout: self, insetForSection: section)
            let sizeHeaderSection = (delegate ?? self).customViewLayout(collectionViewLayout: self, sizeForHeaderSection: section)
            
            let itemCount = collectionView.numberOfItems(inSection: section)
            
            if sizeHeaderSection.width > 0 && sizeHeaderSection.height > 0 && itemCount > 0 {
                var frame: CGRect = CGRect()
                if scrollDirection == .vertical {
                    additionalSectionSpacing += sectionInset.top
                    frame = CGRect(x: 0, y: additionalSectionSpacing, width: sizeHeaderSection.width, height: sizeHeaderSection.height)
                } else {
                    //.horizontal
                    if self.turn_Right() {
                        additionalSectionSpacing += sectionInset.left
                        frame = CGRect(x: 0, y: additionalSectionSpacing, width: sizeHeaderSection.width, height: sizeHeaderSection.height)
                    }else {
                        additionalSectionSpacing += sectionInset.right
                        frame = CGRect(x: 0, y: additionalSectionSpacing, width: sizeHeaderSection.width, height: sizeHeaderSection.height)
                    }
                }
                
                let headerLayoutAttribute = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, with: IndexPath(item: 0, section: section))
                headerLayoutAttribute.frame = frame
                headerLayoutAttribute.zIndex = section * 1000
                
                listHeaderAttributesCache.append(headerLayoutAttribute)
                
                additionalSectionSpacing += frame.height
            }
            
            if sizeHeaderSection.width > 0 && sizeHeaderSection.height > 0 {
                if (scrollDirection == .vertical){
                    additionalSectionSpacing += CGFloat(self.spaceCell)
                }else{
                    //.horizontal
                    if self.turn_Right() {
                        additionalSectionSpacing += CGFloat(self.spaceCell)
                    }else {
                        additionalSectionSpacing += CGFloat(self.spaceCell)
                    }
                }
            }
            
            let columns = (delegate ?? self).customViewLayout(collecitionViewLayout: self, numberColsInSection: section)
            if self.maxColumns < columns {
                self.maxColumns = columns
            }
            
            var itemSize: CGSize = CGSize.init()
            for item in 0..<itemCount {
                let indexPath = IndexPath(item: item, section: section)
                itemSize = (delegate ?? self).customViewLayout(collecitionViewLayout: self, sizeForItem: indexPath)
                
                var frame: CGRect = CGRect.init()
                if scrollDirection == .vertical {
                    let col = (indexPath.row % columns)
                    let row = (indexPath.row / columns)
                    let x : CGFloat = CGFloat(col) * CGFloat(widthCell + spaceCell)
                    let y : CGFloat = CGFloat(row) * CGFloat(heightCell + spaceCell) + additionalSectionSpacing
                    frame = CGRect(x: x, y: y, width: CGFloat(widthCell), height: CGFloat(heightCell))
                } else {
                    //.horizontal
                    if self.turn_Right() {
                        let col = (indexPath.row % columns)
                        let row = (indexPath.row / columns)
                        let x : CGFloat = CGFloat(col) * CGFloat(widthCell + spaceCell)
                        let y : CGFloat = CGFloat(row) * CGFloat(heightCell + spaceCell) + additionalSectionSpacing
                        frame = CGRect(x: x, y: y, width: CGFloat(widthCell), height: CGFloat(heightCell))
                    }else {
                        let col = (indexPath.row % columns)
                        let row = (indexPath.row / columns)
                        let x : CGFloat = CGFloat(col) * CGFloat(widthCell + spaceCell)
                        let y : CGFloat = CGFloat(row) * CGFloat(heightCell + spaceCell) + additionalSectionSpacing
                        frame = CGRect(x: x, y: y, width: CGFloat(widthCell), height: CGFloat(heightCell))
                    }
                }
                
                let itemLayoutAttribute = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                itemLayoutAttribute.frame = frame
                itemLayoutAttribute.zIndex = section * 1000 + item
                
                listCellAttributesCache.append(itemLayoutAttribute)
            }
            
            if (scrollDirection == .vertical){
                if (itemCount % columns) == 0 {
                    additionalSectionSpacing += CGFloat((Int(itemSize.height)+Int(self.spaceCell))*(itemCount / columns))
                }else{
                    additionalSectionSpacing += CGFloat((Int(itemSize.height)+Int(self.spaceCell))*((itemCount / columns)+1))
                }
            }else{
                //.horizontal
                if self.turn_Right() {
                    if (itemCount % columns) == 0 {
                        additionalSectionSpacing += CGFloat((Int(itemSize.height)+Int(self.spaceCell))*(itemCount / columns))
                    }else{
                        additionalSectionSpacing += CGFloat((Int(itemSize.height)+Int(self.spaceCell))*((itemCount / columns)+1))
                    }
                }else {
                    if (itemCount % columns) == 0 {
                        additionalSectionSpacing += CGFloat((Int(itemSize.height)+Int(self.spaceCell))*(itemCount / columns))
                    }else{
                        additionalSectionSpacing += CGFloat((Int(itemSize.height)+Int(self.spaceCell))*((itemCount / columns)+1))
                    }
                }
            }
            
            let sizeFooterSection = (delegate ?? self).customViewLayout(collectionViewLayout: self, sizeForFooterSection: section)
            
            if sizeFooterSection.width > 0 && sizeFooterSection.height > 0 && itemCount > 0 {
                var frame: CGRect = CGRect()
                if scrollDirection == .vertical {
                    additionalSectionSpacing += sectionInset.top
                    frame = CGRect(x: 0, y: additionalSectionSpacing, width: sizeFooterSection.width, height: sizeFooterSection.height)
                } else {
                    //.horizontal
                    if self.turn_Right() {
                        additionalSectionSpacing += sectionInset.left
                        frame = CGRect(x: 0, y: additionalSectionSpacing, width: sizeFooterSection.width, height: sizeFooterSection.height)
                    }else {
                        additionalSectionSpacing += sectionInset.right
                        frame = CGRect(x: 0, y: additionalSectionSpacing, width: sizeFooterSection.width, height: sizeFooterSection.height)
                    }
                }
                
                let footerLayoutAttribute = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, with: IndexPath(item: 0, section: section))
                footerLayoutAttribute.frame = frame
                footerLayoutAttribute.zIndex = section * 1000
                
                listFooterAttributesCache.append(footerLayoutAttribute)
                
                additionalSectionSpacing += frame.height
            }
            
            if sizeFooterSection.width > 0 && sizeFooterSection.height > 0 {
                if (scrollDirection == .vertical){
                    additionalSectionSpacing += sectionInset.bottom
                }else{
                    //.horizontal
                    if self.turn_Right() {
                        additionalSectionSpacing += sectionInset.right
                    }else {
                        additionalSectionSpacing += sectionInset.left
                    }
                }
            }
        }//End For Section
        
        if scrollDirection == .vertical {
            heightContent = additionalSectionSpacing
            widthContent = CGFloat(self.maxColumns * (Int(self.widthCell) + Int(self.spaceCell)))
        } else {
            //.horizontal
            if self.turn_Right() {
                heightContent = additionalSectionSpacing
                widthContent = CGFloat(self.maxColumns * (Int(self.widthCell) + Int(self.spaceCell)))
            }else {
                heightContent = additionalSectionSpacing
                widthContent = CGFloat(self.maxColumns * (Int(self.widthCell) + Int(self.spaceCell)))
            }
        }
    }
    
    public override var collectionViewContentSize: CGSize{
        return CGSize(width: widthContent, height: heightContent)
    }
    
    public override func layoutAttributesForSupplementaryView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        if elementKind == UICollectionView.elementKindSectionHeader {
            return listHeaderAttributesCache.first{ $0.indexPath == indexPath }
        }else if elementKind == UICollectionView.elementKindSectionFooter {
            return listFooterAttributesCache.first{ $0.indexPath == indexPath }
        }
        return nil
    }
    
    public override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return listCellAttributesCache.first {
            return $0.indexPath == indexPath
        }
    }
    
    public override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let headerInRect = listHeaderAttributesCache.filter { (header) -> Bool in
            header.frame.intersects(rect)
        }
        let itemInRect = listCellAttributesCache.filter { (item) -> Bool in
            item.frame.intersects(rect)
        }
        let footerInRect = listFooterAttributesCache.filter { (footer) -> Bool in
            footer.frame.intersects(rect)
        }
        return headerInRect + itemInRect + footerInRect
    }
    
    public override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        if scrollDirection == .vertical, let oldWidth = collectionView?.bounds.width {
            print("BaseCollectionViewLayout: shouldInvalidateLayout: vertical")
            return oldWidth != newBounds.width
        }
        if scrollDirection == .horizontal, let oldHeight = collectionView?.bounds.height {
            print("BaseCollectionViewLayout: shouldInvalidateLayout: horizontal")
            return oldHeight != newBounds.height
        }
        return false
        //return true //alway change --> prepare layout
    }
    
    public override func invalidateLayout() {
        super.invalidateLayout()
        listCellAttributesCache = []
        listHeaderAttributesCache = []
        listFooterAttributesCache = []
        widthContent = 0
        heightContent = 0
    }
}
