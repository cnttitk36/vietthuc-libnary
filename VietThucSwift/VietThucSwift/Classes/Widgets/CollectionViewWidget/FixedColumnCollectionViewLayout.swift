//
//  FixedColumnCollectionViewLayout.swift
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

public class FixedColumnCollectionViewLayout: UICollectionViewLayout {
    public var numberOfColumns = 8 //"Số lượng cột"
    public var itemStatic: [CGSize] = [] //"Mảnh kích thước Item cell"
    
    // "Mảng thuộc tính ứng với các Section". (Section == "Dòng")
    // itemAttributes[0] --> Thuộc tính Dòng 0
    // itemAttributes[0] --> Hay là Mảng thuộc tính các Item của các cột trên Dòng 0
    public var itemAttributes = [[UICollectionViewLayoutAttributes]]()
    
    // "Mảng kích thước Item ứng với các cột". (itemsSize[0] --> Kích thước item cột 0)
    public var itemsSizeDynamic = [CGSize]()
    
    // "Kích thước toàn bộ khung nhìn CollectionView". --> Kích thước vượt qua màn hình
    public var contentSize: CGSize = .zero
    
    public override func prepare() {
        guard let collectionView = collectionView else {
            return
        }
        
        if collectionView.numberOfSections == 0 {
            return
        }
        
        //Nếu "Mảng thuộc tính ứng với các Section" != "Số lượng Section" (Section == "Dòng") --> Tính toán lại
        if self.itemAttributes.count != collectionView.numberOfSections {
            self.generateItemAttributes(collectionView: collectionView)
            return
        }
        
        // Duyệt từng Section
        for section in 0..<collectionView.numberOfSections {
            // Duyệt từng Item ứng với các Section
            for item in 0..<collectionView.numberOfItems(inSection: section) {
                // Bỏ qua nếu Item đầu tiên
                //if section != 0 && item != 0 {
                //    continue
                //}
                
                // Lấy thuộc tính của Item ứng với IndexPath --> Trên mảng đã được tính toán
                let attributes = self.layoutAttributesForItem(at: IndexPath(item: item, section: section))!
                
                // Cố định Dòng
                if section == 0 {
                    var frame = attributes.frame
                    frame.origin.y = collectionView.contentOffset.y
                    attributes.frame = frame
                }
                
                // Cố định cột
                if item == 0 {
                    var frame = attributes.frame
                    frame.origin.x = collectionView.contentOffset.x
                    attributes.frame = frame
                }
                if item == 1 {
                    var frame = attributes.frame
                    frame.origin.x = collectionView.contentOffset.x + self.itemsSizeDynamic[0].width
                    attributes.frame = frame
                }
            }
        }
    }
    
    // Trả về "Kích thước toàn bộ khung nhìn CollectionView". --> Kích thước vượt qua màn hình
    public override var collectionViewContentSize: CGSize {
        return self.contentSize
    }
    
    // Trả về thuộc tính của Item ứng với IndexPath
    // Ứng với Dòng và Cột trên "Mảng thuộc tính ứng với các Section". (Section == "Dòng")
    public override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return self.itemAttributes[indexPath.section][indexPath.row]
    }
    
    // Trả về vị trí của các Item theo "Mảng thuộc tính ứng với các Section" đã được tính toán
    public override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var attributes = [UICollectionViewLayoutAttributes]()
        for section in self.itemAttributes {
            let filteredArray = section.filter( { (layoutAttributes) -> Bool in
                return rect.intersects(layoutAttributes.frame)
            })
            attributes.append(contentsOf: filteredArray)
        }
        return attributes
    }
    
    public override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    
    public override func invalidateLayout() {
        super.invalidateLayout()
        self.itemAttributes = []
        self.contentSize = .zero
        self.itemsSizeDynamic = []
        self.prepare()
    }
}

public extension FixedColumnCollectionViewLayout {
    
    // Tính toán "Mảng thuộc tính ứng với các Section". (Section == "Dòng")
    // Mỗi phần tử --> là "Mảng thuộc Item tính ứng với các Cột"
    public func generateItemAttributes(collectionView: UICollectionView) {
        //Nếu "Mảng kích thước Item ứng với các cột" != "Số lượng cột" --> Tính toán lại "Mảng kích thước Item ứng với các cột"
        if self.itemsSizeDynamic.count != self.numberOfColumns {
            self.calculateItemSizes()
        }
        
        var column = 0 // Lưu vết số lượng cột.
        var xOffset: CGFloat = 0 // Lưu vết X --> Tính chiều rộng khung nhìn CollectionView.
        var yOffset: CGFloat = 0 // Lưu vết Y --> Tính chiều cao khung nhìn CollectionView.
        var contentWidth: CGFloat = 0 // Tổng hợp chiều rộng khung nhìn CollectionView.
        
        // Khởi tạo "Mảng thuộc tính ứng với các Section". (Section == "Dòng")
        self.itemAttributes = []
        
        // Duyệt từng Section. (Duyệt từng Dòng)
        for section in 0..<collectionView.numberOfSections {
            // "Mảng thuộc tính Cột trên mỗi Dòng"
            var sectionAttributes: [UICollectionViewLayoutAttributes] = []
            
            // Duyệt từng Row trong mỗi Section. (Duyệt từng Cột trong mỗi Dòng)
            for index in 0..<self.numberOfColumns {
                //Lấy kích thước Item tương ứng với cột
                let itemSize = self.itemsSizeDynamic[index]
                let indexPath = IndexPath(item: index, section: section)
                let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                attributes.frame = CGRect(x: xOffset, y: yOffset, width: itemSize.width, height: itemSize.height).integral
                
                // Tính toán vị trí nổi của các Item trên Cột & Dòng cố định
                if section == 0 && index == 0 { //Cố định (0,0)
                    attributes.zIndex = 1024
                }else if section == 0 && index == 1 { //Cố định (0,1)
                    attributes.zIndex = 1023
                }else if section == 0 || index <= 1 { //Dòng đầu tiên hoặc cột <= 1
                    attributes.zIndex = 1020
                }
                
                // Nếu Dòng 0 --> cố định origin.y
                if section == 0 {
                    var frame = attributes.frame
                    frame.origin.y = collectionView.contentOffset.y
                    attributes.frame = frame
                }
                
                // Nếu Cột 0 --> cố định origin.x
                if index == 0 {
                    var frame = attributes.frame
                    frame.origin.x = collectionView.contentOffset.x
                    attributes.frame = frame
                }
                
                // Lưu "Mảng thuộc tính Cột trên mỗi Dòng"
                sectionAttributes.append(attributes)
                // Lưu vết X
                xOffset += itemSize.width
                // Lưu vết số lượng cột
                column += 1
                //FixedColumnCollectionViewLayout
                // Nếu là Cột cuối cùng --> Tính toán khung nhìn và reset lại số lượng Cột và độ dài Dòng
                if column == self.numberOfColumns {
                    // Nếu chiều rộng của Dòng > Chiều rộng của khung nhìn CollectionView thì cập nhật lại
                    if xOffset > contentWidth {
                        contentWidth = xOffset
                    }
                    // Reset lại số lượng cột và chiều rộng Dòng --> Để bắt đầu tính toán Dòng tiếp theo
                    column = 0
                    xOffset = 0
                    // Lưu vết Y --> Tính chiều cao khung nhìn CollectionView.
                    yOffset += itemSize.height
                }
            }
            // Lưu "Mảng thuộc tính ứng với các Section". (Section == "Dòng")
            self.itemAttributes.append(sectionAttributes)
        }
        
        // Tính toán kích thước toàn bộ khung nhìn CollectionView
        if let attributes = itemAttributes.last?.last {
            self.contentSize = CGSize(width: contentWidth, height: attributes.frame.maxY)
        }
    }
    
    // Tính toán Mảng kích thước item ứng với các cột
    public func calculateItemSizes() {
        self.itemsSizeDynamic = []
        for index in 0..<self.numberOfColumns {
            self.itemsSizeDynamic.append(self.sizeForItemWithColumnIndex(index))
        }
    }
    
    // Kích thước cho item ở mỗi cột --> cố định chiều cao 30.0
    public func sizeForItemWithColumnIndex(_ columnIndex: Int) -> CGSize {
        if self.itemStatic.count > columnIndex {
            return self.itemStatic[columnIndex]
        }
        var text: NSString
        switch columnIndex {
        case 0:
            text = "MMM-99"
        case 1:
            text = "MMM-99"
        default:
            text = "ContentContent"
        }
        let size: CGSize = text.size(withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14.0)])
        let width: CGFloat = size.width + 16
        return CGSize(width: width, height: 30.0)
    }
}
