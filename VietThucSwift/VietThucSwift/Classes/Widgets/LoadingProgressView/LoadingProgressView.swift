//
//  LoadingProgressView.swift
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

public class LoadingProgressView: UIView {
    
    @IBOutlet weak var _IndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var _baseView: UIView!
    @IBOutlet weak var _title: UILabel!
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        let podBundle = Bundle(for: type(of: self))
        if let bundleURL = podBundle.url(forResource: "VietThucSwift", withExtension: "bundle") {
            if let bundle = Bundle(url: bundleURL) {
                if let view = UINib(nibName: String(describing: type(of: self)), bundle: bundle).instantiate(withOwner: self, options: nil).first as? UIView {
                    view.frame = UIScreen.main.bounds
                    view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
                    addSubview(view)
                    backgroundColor = UIColor.clear
                    _baseView.layer.cornerRadius = 10.0
                    _baseView.layer.masksToBounds = true
                    _IndicatorView.transform = CGAffineTransform(scaleX: 2.50, y: 2.50);
                    _IndicatorView.startAnimating()
                }
            }
        }
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    public func updateStatus(value: String) {
        _title.text = value
    }
}

