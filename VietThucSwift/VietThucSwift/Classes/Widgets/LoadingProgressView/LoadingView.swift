//
//  LoadingView.swift
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit 

public class LoadingView: UIView {
    
    fileprivate let spinner = UIActivityIndicatorView(style: .gray)
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        spinner.transform = CGAffineTransform(scaleX: 2.75, y: 2.75);
        spinner.startAnimating()
        addSubview(spinner)
        backgroundColor = UIColor.clear
    }
    
    @available(*, unavailable)
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        spinner.center = center
    }
}
