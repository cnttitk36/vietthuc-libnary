//
//  TableViewWidget.swift
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

public enum StyleSwipe {
    case LEFT
    case LEFT_TOP
    case LEFT_BOTTOM
    case LEFT_1
    case LEFT_2
    case LEFT_3
    case RIGHT
    case RIGHT_TOP
    case RIGHT_BOTTOM
    case RIGHT_1
    case RIGHT_2
    case RIGHT_3
}

private extension String {
    var firstUppercased: String {
        guard let first = first else { return "" }
        return String(first).uppercased() + dropFirst()
    }
}

//Animation for cell
private extension UITableView {
    func isLastVisibleCell(at indexPath: IndexPath) -> Bool {
        guard let lastIndexPath = indexPathsForVisibleRows?.last else {
            return false
        }
        return lastIndexPath == indexPath
    }
}

public class Animator {
    public var hasAnimatedAllCells = false
    public let animation: Animation
    
    public init(animation: @escaping Animation) {
        self.animation = animation
    }
    
    public func animate(cell: UITableViewCell, at indexPath: IndexPath, in tableView: UITableView) {
        guard !hasAnimatedAllCells else { return }
        animation(cell, indexPath, tableView)
        hasAnimatedAllCells = tableView.isLastVisibleCell(at: indexPath)
    }
}

public typealias Animation = (UITableViewCell, IndexPath, UITableView) -> Void
public enum AnimationFactory {
    // Animation Cell hiện ra đậm hơn
    public static func makeFadeAnimation(duration: TimeInterval, delayFactor: Double) -> Animation {
        return { cell, indexPath, _ in
            cell.alpha = 0
            UIView.animate(
                withDuration: duration,
                delay: delayFactor * Double(indexPath.row),
                animations: {
                    cell.alpha = 1
            })
        }
    }
    
    // Animation Cell di chuyển + hiện ra đậm hơn
    public static func makeMoveUpWithFade(rowHeight: CGFloat, duration: TimeInterval, delayFactor: Double) -> Animation {
        return { cell, indexPath, _ in
            cell.transform = CGAffineTransform(translationX: 0, y: rowHeight / 2)
            cell.alpha = 0
            UIView.animate(
                withDuration: duration,
                delay: delayFactor * Double(indexPath.row),
                options: [.curveEaseInOut],
                animations: {
                    cell.transform = CGAffineTransform(translationX: 0, y: 0)
                    cell.alpha = 1
            })
        }
    }
    
    // Animation Cell di chuyển các từ cạnh phải của màn hình
    public static func makeSlideIn(duration: TimeInterval, delayFactor: Double) -> Animation {
        return { cell, indexPath, tableView in
            cell.transform = CGAffineTransform(translationX: tableView.bounds.width, y: 0)
            
            UIView.animate(
                withDuration: duration,
                delay: delayFactor * Double(indexPath.row),
                options: [.curveEaseInOut],
                animations: {
                    cell.transform = CGAffineTransform(translationX: 0, y: 0)
            })
        }
    }
}
//Animation for cell ./

public protocol TableViewDelegateWidget {
    func numberOfSections(in tableView: UITableView) -> Int? //require
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat?
    func tableView(_ tableView: UITableView, nibForHeaderInSection section: Int) -> String?
    func tableView(_ tableView: UITableView,_ header: UITableViewCell?, viewForHeaderInSection section: Int) -> UIView?
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat?
    func tableView(_ tableView: UITableView, nibForFooterInSection section: Int) -> String?
    func tableView(_ tableView: UITableView,_ footer: UITableViewCell?, viewForFooterInSection section: Int) -> UIView?
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat?
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat?
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int?
    func tableView(_ tableView: UITableView, nibForRowAt indexPath: IndexPath) -> String? //require
    func tableView(_ tableView: UITableView,_ row: UITableViewCell, cellForRowAt indexPath: IndexPath) -> UITableViewCell? //require
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) //require
    func tableView(_ cell: UITableViewCell, didTapSwipe styleSwipe: StyleSwipe)
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool?
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath)
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
}

public extension TableViewDelegateWidget {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat? {return nil}
    func tableView(_ tableView: UITableView, nibForHeaderInSection section: Int) -> String? {return nil}
    func tableView(_ tableView: UITableView,_ header: UITableViewCell?, viewForHeaderInSection section: Int) -> UIView? {return nil}
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat? {return nil}
    func tableView(_ tableView: UITableView, nibForFooterInSection section: Int) -> String? {return nil}
    func tableView(_ tableView: UITableView,_ footer: UITableViewCell?, viewForFooterInSection section: Int) -> UIView? {return nil}
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat? {return nil}
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat? {return nil}
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int? {return nil}
    
    func tableView(_ cell: UITableViewCell, didTapSwipe styleSwipe: StyleSwipe) {}
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        // Animation Cell hiện ra đậm hơn
        //let animation = AnimationFactory.makeFadeAnimation(duration: 0.5, delayFactor: 0.05)
        //let animator = Animator(animation: animation)
        //animator.animate(cell: cell, at: indexPath, in: tableView)
        
        // Animation Cell di chuyển + hiện ra đậm hơn
        //let animation = AnimationFactory.makeMoveUpWithFade(rowHeight: cell.frame.height, duration: 0.5, delayFactor: 0.05)
        //let animator = Animator(animation: animation)
        //animator.animate(cell: cell, at: indexPath, in: tableView)
        
        // Animation Cell di chuyển các từ cạnh phải của màn hình
        //let animation = AnimationFactory.makeSlideIn(duration: 0.5, delayFactor: 0.05)
        //let animator = Animator(animation: animation)
        //animator.animate(cell: cell, at: indexPath, in: tableView)
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool? {return nil}
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {return nil}
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {}
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {}
}

public class TableViewWidget: UITableView, UITableViewDelegate, UITableViewDataSource, SwipeStyle1CellDelegate, SwipeStyle2CellDelegate {
    private var tableViewDelegateWidget: TableViewDelegateWidget?
    private var listData: [Any?]?
    private var heighRow: CGFloat?
    
    public override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)//UITableViewStyle.grouped - UITableViewStyle.plain
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //If listData == nil && heighRow == nil <--> require implement TableViewDelegateWidget
    //init(frame: CGRect, listNib: [String], listData: nil, heighRow: nil, tableViewDelegateWidget: self, style: nil)
    //init(frame: CGRect, listNib: [String], listData: [AnyObject?], heighRow: 60, tableViewDelegateWidget: nil, style: nil)
    public func initRegistry(_ listNib: [String],_ listData: [Any?]?,_ heighRow: CGFloat?,_ tableViewDelegateWidget: TableViewDelegateWidget?) {
        for nib in listNib {
            register(UINib(nibName: nib.firstUppercased, bundle: nil), forCellReuseIdentifier: nib)
        }
        if let list = listData{
            self.listData = list
        }
        if let heighRow = heighRow{
            self.heighRow = heighRow
        }
        if let delegate = tableViewDelegateWidget {
            self.tableViewDelegateWidget = delegate
        }
        self.delegate = self
        self.dataSource = self
        self.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: CGFloat.leastNormalMagnitude))
        self.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: CGFloat.leastNormalMagnitude))
        self.setContentOffset(CGPoint.zero, animated: true)
    }
    
    public func updateListData(_ listData: [AnyObject?]?) {
        if let list = listData{
            self.listData = list
            reloadData()
        }
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return self.tableViewDelegateWidget?.numberOfSections(in: tableView) ?? 0
    }
    
    //Header
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return self.tableViewDelegateWidget?.tableView(tableView, heightForHeaderInSection: section) ?? 0.0
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let nibHeader = self.tableViewDelegateWidget?.tableView(tableView, nibForHeaderInSection: section) {
            if let headerView = tableView.dequeueReusableCell(withIdentifier: nibHeader) {//UITableViewCell
                headerView.selectionStyle = .none
                if let headerRow = self.tableViewDelegateWidget?.tableView(tableView, headerView, viewForHeaderInSection: section) {
                    headerRow.layoutIfNeeded()
                    return headerRow
                }
                headerView.layoutIfNeeded()
                return self.tableViewDelegateWidget?.tableView(tableView, headerView, viewForHeaderInSection: section)
            }
        }
        return self.tableViewDelegateWidget?.tableView(tableView, nil, viewForHeaderInSection: section)
    }
    
    //Header ./
    
    //Footer
    public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return self.tableViewDelegateWidget?.tableView(tableView, heightForFooterInSection: section) ?? 0.0
    }
    
    public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if let nibFooter = self.tableViewDelegateWidget?.tableView(tableView, nibForFooterInSection: section) {
            if let footerView = tableView.dequeueReusableCell(withIdentifier: nibFooter) {//UITableViewCell
                footerView.selectionStyle = .none
                if let footerRow = self.tableViewDelegateWidget?.tableView(tableView, footerView, viewForFooterInSection: section) {
                    footerRow.layoutIfNeeded()
                    return footerRow
                }
                footerView.layoutIfNeeded()
                return self.tableViewDelegateWidget?.tableView(tableView, footerView, viewForFooterInSection: section)
            }
        }
        return self.tableViewDelegateWidget?.tableView(tableView, nil, viewForFooterInSection: section)
    }
    //Footer ./
    
    //Row
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = self.tableViewDelegateWidget?.tableView(tableView, heightForRowAt: indexPath) {
            return height
        }else if let height = self.heighRow {
            return height
        }
        return 0.0
    }
    
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = self.tableViewDelegateWidget?.tableView(tableView, estimatedHeightForRowAt: indexPath) {
            return height
        }else if let height = self.heighRow {
            return height
        }
        return 0.0
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let size = self.tableViewDelegateWidget?.tableView(tableView, numberOfRowsInSection: section) {
            return size
        }else if let size = self.listData?.count {
            return size
        }
        return 0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let nibRow = self.tableViewDelegateWidget?.tableView(tableView, nibForRowAt: indexPath) {
            if let rowView = tableView.dequeueReusableCell(withIdentifier: nibRow) {//UITableViewCell
                rowView.selectionStyle = .none
                if let cellRow = self.tableViewDelegateWidget?.tableView(tableView, rowView, cellForRowAt: indexPath) as? SwipeStyle1Cell {
                    cellRow.delegate = self
                    cellRow.layoutIfNeeded()
                    return cellRow
                }
                if let cellRow = self.tableViewDelegateWidget?.tableView(tableView, rowView, cellForRowAt: indexPath) as? SwipeStyle2Cell {
                    cellRow.delegate = self
                    cellRow.layoutIfNeeded()
                    return cellRow
                }
                if let row = self.tableViewDelegateWidget?.tableView(tableView, rowView, cellForRowAt: indexPath) {
                    row.layoutIfNeeded()
                    return row
                }
            }
        }
        return UITableViewCell()
    }
    //Row ./
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Log.debug("TableView Section: \(indexPath.section) - RowCell: \(indexPath.row)")
        self.tableViewDelegateWidget?.tableView(tableView, didSelectRowAt: indexPath)
    }
    
    //Swipe Style1
    public func DidTapSwipeBegin_Style1(cell: SwipeStyle1Cell, indexPath: IndexPath) {
        Log.debug("Did Tap Swipe Begin Style 1")
    }
    
    public func DidTapLeftButtonDetail_Style1(cell: SwipeStyle1Cell, indexPath: IndexPath) {
        Log.debug("Did Tap Left Button Detail")
        self.tableViewDelegateWidget?.tableView(cell, didTapSwipe: StyleSwipe.LEFT)
    }
    
    public func DidTapRightButtonTop_Style1(cell: SwipeStyle1Cell, indexPath: IndexPath) {
        Log.debug("Did Tap Right Button Top")
        self.tableViewDelegateWidget?.tableView(cell, didTapSwipe: StyleSwipe.RIGHT_TOP)
    }
    
    public func DidTapRightButtonBottom_Style1(cell: SwipeStyle1Cell, indexPath: IndexPath) {
        Log.debug("Did Tap Right Button Bottom")
        self.tableViewDelegateWidget?.tableView(cell, didTapSwipe: StyleSwipe.RIGHT_BOTTOM)
    }
    //Swipe Style1 ./
    
    //Swipe Style2
    public func DidTapSwipeBegin_Style2(cell: SwipeStyle2Cell, indexPath: IndexPath) {
        Log.debug("Did Tap Swipe Begin Style 2")
    }
    
    public func DidTapRightButton_1_Style2(cell: SwipeStyle2Cell, indexPath: IndexPath) {
        Log.debug("Did Tap Right Button 1")
        self.tableViewDelegateWidget?.tableView(cell, didTapSwipe: StyleSwipe.RIGHT_1)
    }
    
    public func DidTapRightButton_2_Style2(cell: SwipeStyle2Cell, indexPath: IndexPath) {
        Log.debug("Did Tap Right Button 2")
        self.tableViewDelegateWidget?.tableView(cell, didTapSwipe: StyleSwipe.RIGHT_2)
    }
    
    public func DidTapRightButton_3_Style2(cell: SwipeStyle2Cell, indexPath: IndexPath) {
        Log.debug("Did Tap Right Button 3")
        self.tableViewDelegateWidget?.tableView(cell, didTapSwipe: StyleSwipe.RIGHT_3)
    }
    
    public func DidTapLeftButton_1_Style2(cell: SwipeStyle2Cell, indexPath: IndexPath) {
        Log.debug("Did Tap Left Button 1")
        self.tableViewDelegateWidget?.tableView(cell, didTapSwipe: StyleSwipe.LEFT_1)
    }
    //Swipe Style2 ./
    
    //ScrollView
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //Log.debug("Did scroll tableview")
        self.tableViewDelegateWidget?.scrollViewDidScroll(scrollView)
    }
    //ScrollView ./
    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.tableViewDelegateWidget?.tableView(tableView, willDisplay: cell, forRowAt: indexPath)
    }
    
    public func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return self.tableViewDelegateWidget?.tableView(tableView, canEditRowAt: indexPath) ?? false
    }
    
    public func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        return self.tableViewDelegateWidget?.tableView(tableView, editActionsForRowAt: indexPath)
    }
    
    public func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        self.tableViewDelegateWidget?.tableView(tableView, commit: editingStyle, forRowAt: indexPath)
    }
    
}

public extension UITableView {
    func scrollToBottom(){
        DispatchQueue.main.async {
            if self.numberOfSections > 0 {
                if self.numberOfRows(inSection:  self.numberOfSections - 1) > 0 {
                    let indexPath = IndexPath(
                        row: self.numberOfRows(inSection:  self.numberOfSections - 1) - 1,
                        section: self.numberOfSections - 1)
                    self.scrollToRow(at: indexPath, at: .bottom, animated: true)
                }
            }
        }
    }
    
    func scrollToTop() {
        DispatchQueue.main.async {
            if self.numberOfSections > 0 {
                if self.numberOfRows(inSection:  self.numberOfSections - 1) > 0 {
                    let indexPath = IndexPath(row: 0, section: 0)
                    self.scrollToRow(at: indexPath, at: .top, animated: false)
                }
            }
        }
    }
}
