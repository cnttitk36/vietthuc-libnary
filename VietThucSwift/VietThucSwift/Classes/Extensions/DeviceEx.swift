//
//  DeviceEx.swift
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import Foundation
import DeviceKit

public extension UIDevice {
    
    static let groupOfSmall: [Device] =
        [Device.iPhone5, Device.iPhone5c, Device.iPhone5s, Device.iPhoneSE,
         Device.simulator(.iPhone5), Device.simulator(.iPhone5c), Device.simulator(.iPhone5s), Device.simulator(.iPhoneSE)]
    static let groupOfMedium: [Device] =
        [Device.iPhone6, Device.iPhone6s, Device.iPhone7, Device.iPhone8,
         Device.simulator(.iPhone6), Device.simulator(.iPhone6s), Device.simulator(.iPhone7), Device.simulator(.iPhone8)]
    static let groupOfLarge: [Device] =
        [Device.iPhone6Plus, Device.iPhone6sPlus, Device.iPhone7Plus, Device.iPhone8Plus,
         Device.simulator(.iPhone6Plus), Device.simulator(.iPhone6sPlus), Device.simulator(.iPhone7Plus), Device.simulator(.iPhone8Plus)]
    static let groupOfLargeX: [Device] =
        [ .iPhoneX, .iPhoneXs, .iPhoneXsMax, .iPhoneXr,
         .simulator(.iPhoneX), .simulator(.iPhoneXs), .simulator(.iPhoneXsMax), .simulator(.iPhoneXr)]
    
    
    func isOneOf(_ devices: [Device]) -> Bool {
        return devices.contains(Device())
    }
    
    func autoSize(_ iPhone5: CGFloat,_ iPhone6: CGFloat,_ iPhone6Plus: CGFloat,_ iPhoneX: CGFloat,_ iPadDefault: CGFloat) -> CGFloat {
        if Device().isOneOf(UIDevice.groupOfSmall) {
            return iPhone5
        } else if Device().isOneOf(UIDevice.groupOfMedium) {
            return iPhone6
        }else if Device().isOneOf(UIDevice.groupOfLarge) {
            return iPhone6Plus
        }else if Device().isOneOf(UIDevice.groupOfLargeX) {
            return iPhoneX
        }
        return iPadDefault
    }
}
