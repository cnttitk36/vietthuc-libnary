//
//  ApplicationEx.swift
//
//  Created by Tran Viet Thuc - 0389955141 on 3/29/19.
//  Copyright © 2019 boys vip. All rights reserved.
//

import UIKit

extension UIApplication {
    
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
    
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
    
    public class var topView: UIView? {
        return UIApplication.shared.keyWindow
    }
    
    public class func getAppVersion() -> String? {
        if let appInfo = Bundle.main.infoDictionary {
            if let versionNumber = appInfo["CFBundleShortVersionString"] as? String {
                return versionNumber
            }
        }
        return nil
    }
    
    public class func openAppStore(_ url: String = "itms-apps://itunes.apple.com/us/app/apple-store/id377298193") {
        if let url = URL(string: "\(url)") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    public class func openSettingsURLStringAppServices() {
        if let url = URL(string: UIApplication.openSettingsURLString) as URL? {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}
