#
# Be sure to run `pod lib lint VietThucSwift.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'VietThucSwift'
  s.version          = '0.3.0'
  s.summary          = 'Swift libnary of Trần Việt Thức. Tel: 038.99.55.141'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
Swift libnary of Trần Việt Thức. Tel: 038.99.55.141. Please install libnary into project, never have to write this generic code again!
                       DESC
  s.homepage         = 'https://gitlab.com/cnttitk36/vietthuc-libnary'
  #s.screenshots      = 'http://mobiapps.vn/images/MobiApp-logo.png', 'http://mobiapps.vn/images/MobiApp-logo.png'
  s.license          = { :type => 'MIT', :file => 'VietThucSwift/LICENSE' }
  #s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Trần Việt Thức' => 'cnttitk36@gmail.com' }
  
  s.swift_version       = '5.0'
  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '5.0' }
  
  
  # ――― Platform Specifics ―――――― #
  s.platform              = :ios
  s.platform              = :ios, "0.0"
  s.ios.deployment_target = '9.0'
  
  s.source           = { :git => 'https://gitlab.com/cnttitk36/vietthuc-libnary.git', :tag => s.version.to_s }
  s.social_media_url = 'http://mobiapps.vn/images/MobiApp-logo.png'
  
  # ======= Stream IO PodFramework.zip
  #s.source = { :http => 'https://<hostname_of_where_you_want_to_host_it>/PodFramework.zip' }
  #s.vendored_frameworks = 'PodFramework.framework'
  #s.vendored_libraries = 'libProj4.a', 'libJavaScriptCore.a'
  #s.source_files = 'Classes/**/*.{h,m}', 'More_Classes/**/*.{h,m}'
  
  #RunPushGit
  s.source_files = 'VietThucSwift/VietThucSwift/Classes/**/*.{swift,xib}'
  s.resource_bundles = {
      'VietThucSwift' => ['VietThucSwift/VietThucSwift/**/*.{xib,mp3,png,jpg}']
  }
  
  #RunDebug
  #s.source_files = 'VietThucSwift/Classes/**/*.{swift,xib}'
  #s.resource_bundles = {
  # 'VietThucSwift' => ['VietThucSwift/**/*.{xib,mp3,png,jpg}']
  #}
  
  
  #s.public_header_files = 'VietThucSwift/**/*.h'
  s.requires_arc = true
  
  
  s.static_framework = true
  s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'DeviceKit', '~> 1.9.0'
  
  #s.dependency 'Alamofire', '~> 4.7.3'
  #s.dependency 'AlamofireImage', '~> 3.4.1'
  #s.dependency 'HandyJSON', '~> 4.2.0'
  #s.dependency 'ESTabBarController-swift', '~> 2.5'
  #s.dependency 'pop', '~> 1.0.12'
  
  #s.dependency 'GooglePlacePicker', '~> 2.7.0'
  #s.dependency 'GooglePlaces', '~> 2.7.0'
  #s.dependency 'GoogleMaps', '~> 2.7.0'
  #s.dependency 'Firebase/Core', '~> 5.10.0'
  #s.dependency 'Firebase/Messaging', '~> 5.10.0'
  #s.dependency 'Firebase/Crash', '~> 5.10.0'
  #s.dependency 'Firebase/RemoteConfig', '~> 5.10.0'
  #s.dependency 'Fabric', '~> 1.7.13'
  #s.dependency 'Crashlytics', '~> 3.10.9'
  
  #s.dependency 'SDWebImage', '~> 4.4.2'
  #s.dependency 'OpalImagePicker', '~> 1.7.1'
  #s.dependency 'ImageCenterButton', '~> 0.1.4'
  #s.dependency 'ImageSlideshow', '~> 1.7.0'
  #s.dependency 'ImageSlideshow/Alamofire', '~> 1.7.0'
  #s.dependency 'ImageSlideshow/AFURL', '~> 1.7.0'
  #s.dependency 'ImageSlideshow/SDWebImage', '~> 1.7.0'
  #s.dependency 'ImageSlideshow/Kingfisher', '~> 1.7.0'
  #s.dependency 'SnapKit', '~> 4.0.1'
  #s.dependency 'KGModal', '~> 1.2.0'
  #s.dependency 'JTAppleCalendar', '~> 7.0'
  #s.dependency 'mailcore2-ios', '~> 0.6.4'
  #s.dependency 'SkyFloatingLabelTextField', '~> 3.6.0'
  
end
